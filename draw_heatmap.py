import numpy as np
from PyQt5.QtWidgets import QApplication, QMainWindow
import sys
import matplotlib.pyplot as plt

from desktop_app.lion_gui.chart import MplCanvas
from fit_functions.fit_manager import FitRatioManager
from measurement.measurement_group import MeasurementGroup
from fit_functions.solution import SolutionManager
from fit_functions.visual_functions import plot_on_grid


def calculate_distance_and_std(x2, y2, std_x, std_y):
    # Calculate the distance from the origin (0, 0) to the point (x2, y2)
    distance = np.sqrt(x2 ** 2 + y2 ** 2)

    # Calculate partial derivatives assuming x1=0 and y1=0
    if distance == 0:
        partial_x = partial_y = 0  # To avoid division by zero in completely degenerate case
    else:
        partial_x = x2 / distance
        partial_y = y2 / distance

    # Calculate the standard deviation of the distance
    std_distance = np.sqrt((partial_x * std_x) ** 2 + (partial_y * std_y) ** 2)

    return distance, std_distance



# Initialize the QApplication
app = QApplication(sys.argv)
window = QMainWindow()
window.setWindowTitle('Plot Example')

# Create an instance of MplCanvas
canvas = MplCanvas(window, width=5, height=5, dpi=100)
window.setCentralWidget(canvas)
window.resize(1000, 1000)
NUMBER_OF_MEASUREMENTS = 10000
# Load the fit managers and solve ratios
x_fit_manager = FitRatioManager.get_from_file('picled_objects/x_fit_manager_new.pkl')
y_fit_manager = FitRatioManager.get_from_file('picled_objects/y_fit_manager_new.pkl')
FOLDER = "measurement_data/data_lion_new/"
DATA_FOLDER = FOLDER
picture_folder = "pictures/pictures_estimation_new/"
# offsets = {"Channel A": 1, "Channel B": 1.2, "Channel C": 1.2, "Channel D": 2.2}
offsets = {"Channel A": 0, "Channel B": 0, "Channel C": 0, "Channel D": 0}
# x_fit_manager = FitRatioManager.get_from_file('x_fit_manager_calib.pkl')
# y_fit_manager = FitRatioManager.get_from_file('y_fit_manager_calib.pkl')

sol_manager = SolutionManager.load_grid_from_file(1000, "picled_objects/solution_manager_lion.pkl")
sol_manager.add_fit_x(x_fit_manager)
sol_manager.add_fit_y(y_fit_manager)
measurement_file = "picled_objects/lion_measurement_new.pkl"
mgroup = MeasurementGroup(offsets, FOLDER, DATA_FOLDER, binary=True)
# mgroup = MeasurementGroup.get_from_file(measurement_file)
mgroup.estimate_position_all_measurements(sol_manager, table=True)
accuracy_x, std_xs, accuracy_y, std_ys = mgroup.get_estimated_accuracy()
d = dict()
d_std = dict()
for key, value in accuracy_x.items():
    d1, d_std1 = calculate_distance_and_std(accuracy_x[key], accuracy_y[key], std_xs[key], std_ys[key])
    d[key] = d1
    d_std[key] = d_std1
plot_on_grid(accuracy_x, std_xs, "plots/x_grid.png")
plot_on_grid(accuracy_y, std_ys, "plots/y_grid.png")
plot_on_grid(d, d_std, "plots/d_grid.png")
plt.show()







# mgroup.save_all_estimations(picture_folder)
# mgroup.save(measurement_file)
# folder = "desktop_app/data_lion/"
# meas = MeasurementObject("pos_15_30_data.dat", folder, 10000)
# meas.get_data_from_binary()
#
# # meas = MeasurementObject("pos_0_0", None, 1000)
# # DATA_FOLDER = "data_last/"
# # meas.get_data_from_files(DATA_FOLDER)
#
# # Update plot for each ratio
# meas.estimate_x_y(sol)
# meas.save_estimated_x_y(filename="plot.png")
# x_ratio, y_ratio = meas.get_single_ratios_normed()
# print(x_ratio.size)
# print(y_ratio.size)
#
# # estimated_x = []
# # estimated_y = []
# estimated_x, estimated_y = sol.solve_ratios(x_ratio, y_ratio)
# meas.estimate_x_y(sol)

# for i in range(10000):
#     est_x, est_y = sol.solve_ratios(x_ratio[i], y_ratio[i])
#     estimated_x.append(est_x)
#     estimated_y.append(est_y)
#
#     # Print the estimates
#     print(f"Estimate X: {est_x}, Estimate Y: {est_y}")
# #     # Update the plot
#
# canvas.update_plot(estimated_x, estimated_y)
#     # Give some time to update GUI
# QApplication.processEvents()
#
# # Show the window
# window.show()
# sys.exit(app.exec_())
