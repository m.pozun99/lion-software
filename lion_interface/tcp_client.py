import socket
import struct
import time
import threading

from commands import *
from measurement.measurement import MeasurementObject

class LionClient:
    DEFAULT_TIMEOUT = 3  # seconds
    DAC_RANGE = (0, 4095)
    ADC_RANGE = (0, 4095)
    MCU_REFERENCE = 2.95  # V
    VBIAS_GAIN = 11

    def __init__(self, server_ip, server_port, filename=None, buffer_size=264):
        self.server_ip = server_ip
        self.server_port = server_port
        self.filename = filename
        self.buffer_size = buffer_size
        self.socket = None
        self.stop_event = threading.Event()
        self.stop_command_sent = False  # Flag to track if stop command has been sent
        self.data_callback = None

    def add_data_callback(self, callback):
        self.data_callback = callback

    def connect_to_server(self):
        """Establishes a socket connection to the server and flushes any existing data."""
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.server_ip, self.server_port))
        self.socket.settimeout(self.DEFAULT_TIMEOUT)  # Set the timeout for blocking socket operations
        print("Connected to server.")

        # Flush the socket by reading any pending data
        self.socket.setblocking(0)  # Set socket to non-blocking to flush it
        try:
            while True:
                data = self.socket.recv(self.buffer_size)
                if not data:
                    break  # No more data, exit loop
        except socket.error as e:
            pass  # Expected exception when no more data is available

        self.socket.setblocking(1)  # Set socket back to blocking mode
        self.socket.settimeout(self.DEFAULT_TIMEOUT)  # Set the timeout for blocking socket operations
        print("Socket flushed.")

    def close_connection(self):
        """Closes the socket connection and any related resources."""
        if self.socket:
            self.socket.close()
            self.socket = None
            print("Connection closed.")

    def receive_and_write_to_file(self):
        with open(self.filename, 'wb') as file:
            data_length = 0
            start_time = None
            stop_command_sent = False
            socket_empty = False
            while not (self.stop_event.is_set() and stop_command_sent and socket_empty):
                try:
                    if self.stop_event.is_set():
                        self.send_stop_command()
                        stop_command_sent = True
                    data = self.socket.recv(self.buffer_size)
                    if data:
                        print("Data received")
                        if self.data_callback:  # Check if the callback function is set
                            self.data_callback(data)  # Call the callback with the received data
                        cmd = bytes([1])
                        self.socket.sendall(cmd)
                        if start_time is None:
                            start_time = time.time()
                        file.write(data)
                        data_length += len(data)
                    else:
                        if stop_command_sent:
                            socket_empty = True
                            break  # If stop command sent and no more data, exit loop
                        continue
                except socket.timeout:
                    print("socket timeout")
                    if stop_command_sent:
                        socket_empty = True
                        break  # If stop command sent and no more data, exit loop
                    continue  # Normal timeout exception, continue receiving
                except Exception as e:
                    print(f"Error during receiving data: {e}")
                    break

            if data_length > 0 and start_time:
                total_time = time.time() - start_time
                print(f"{data_length} bytes received at {data_length / total_time} bytes/sec")

    def start_measurement(self, filename=None):
        if filename is not None:
            self.filename = filename
        print(self.filename)
        print("START MEASUREMENT")
        self.stop_event.clear()
        self.stop_command_sent = False
        cmd = START_MEASUREMENT
        self.send_cmd(cmd, 1)
        self.recv_thread = threading.Thread(target=self.receive_and_write_to_file)
        self.recv_thread.start()

    def stop_measurement(self):
        print("STOP MEASUREMENT")
        # self.send_stop_command()  # Ensure the server is notified to stop sending data
        self.stop_event.set()
        self.recv_thread.join()  # Wait for the receiving thread to finish
        print("Measurement stopped and thread terminated.")

    def send_cmd(self, cmd: bytes, rsp_len: int) -> bytes:
        """
        Sends a command via TCP/IP and waits for a response.
        """
        try:
            self.socket.sendall(cmd)
            print(f"Command sent: {cmd}")
            if rsp_len > 0:
                response = self.socket.recv(rsp_len)
                print(f"Response received: {response}")
                return response
        except Exception as err:
            print(f"Error sending command: {err}")

    def send_stop_command(self):
        """Sends the 'stop' command to the server."""
        try:
            stop_cmd = STOP_MEASUREMENT  # Define this based on your protocol
            self.socket.sendall(stop_cmd)
            print("Stop command sent.")
        except Exception as e:
            print("Error sending stop command:", e)

    @staticmethod
    def _check_response(response):
        """Checks the response from the server for success."""
        print(response[0], "resp")
        return len(response) == 1 and response[0] == 1

    # DAC
    def set_vbias_setting(self, value):
        cmd = SET_VBIAS + struct.pack("<H", value)
        resp = self.send_cmd(cmd, 1)
        return self._check_response(resp)

    def get_vbias_setting(self):
        cmd = GET_VBIAS
        resp = self.send_cmd(cmd, 2)
        value = struct.unpack("<H", resp)[0]
        return value

    def set_vthresh_setting(self, value):
        cmd = SET_VTHRESH + struct.pack("<H", value)
        resp = self.send_cmd(cmd, 1)
        return self._check_response(resp)

    def get_vthresh_setting(self):
        cmd = GET_VTHRESH
        resp = self.send_cmd(cmd, 2)
        value = struct.unpack("<H", resp)[0]
        return value

    def calc_vbias_value_from_setting(self, dac_setting):
        vbias = dac_setting / self.DAC_RANGE[1] * self.MCU_REFERENCE * self.VBIAS_GAIN
        return vbias

    def calc_vbias_setting_from_value(self, value):
        dac_setting = round(value / self.VBIAS_GAIN * self.DAC_RANGE[1] / self.MCU_REFERENCE)
        return dac_setting

    def calc_vthresh_value_from_setting(self, dac_setting):
        vthresh = dac_setting / self.DAC_RANGE[1] * self.MCU_REFERENCE
        return vthresh

    def calc_vthresh_setting_from_value(self, value):
        dac_setting = round(value * self.DAC_RANGE[1] / self.MCU_REFERENCE)
        return dac_setting

if __name__ == "__main__":
    server_ip = '10.42.0.244'
    # server_ip = '192.168.1.200'

    server_port = 10001
    filename = 'output.dat'
    FOLDER = "data_amp_new/"
    client = LionClient(server_ip, server_port, filename)
    client.connect_to_server()
    print(client.get_vbias_setting())
    client.set_vthresh_setting(2300)
    for vbias in range(3340, 3630, 20):
        print(client.set_vbias_setting(vbias))
        print(client.get_vbias_setting())
        name = f"amp305_bias_{vbias}.dat"
        filename = FOLDER + name
        client.start_measurement(filename)
        time.sleep(10)  # simulate measurement time
        client.stop_measurement()
        time.sleep(0.5)        # meas = MeasurementObject(name, FOLDER)
        # meas.get_data_from_binary()
        # meas.plot_all_channel_histograms(show=True)
        print(client.get_vbias_setting())
        # meas = MeasurementObject(name, FOLDER)
        # meas.get_data_from_binary()
        # meas.plot_all_channel_histograms(show=True)

