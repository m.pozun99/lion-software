import socket
import time
import threading
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout

class DataReceiver:
    def __init__(self, server_ip, server_port, filename):
        self.server_ip = server_ip
        self.server_port = server_port
        self.filename = filename
        self.running = True  # Control the loop
        self.socket = None

    def start_receiving(self):
        threading.Thread(target=self.receive_and_write_to_file, daemon=True).start()

    def receive_and_write_to_file(self):
        buffer_size = 80
        buffer = bytearray()

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            self.socket = s
            s.connect((self.server_ip, self.server_port))

            with open(self.filename, 'wb') as file:
                data_length = 0
                first = True
                while self.running:
                    data = s.recv(10)
                    if first:
                        t1 = time.time()
                        first = False
                    if not data:
                        break

                    buffer.extend(data)

                    if len(buffer) >= buffer_size:
                        data_length += len(buffer)
                        t2 = time.time()
                        print(data_length, data_length / (t2 - t1))
                        file.write(buffer)
                        buffer.clear()

                if buffer:
                    file.write(buffer)

    def send_command(self, command):
        if self.socket:
            self.socket.send(command.encode())

    def stop(self):
        self.running = False
        if self.socket:
            self.socket.close()
