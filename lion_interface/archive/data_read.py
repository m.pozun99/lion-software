import struct
import matplotlib.pyplot as plt

def is_valid_header(letter, count):
    """Check if the header is valid based on the letter and count."""
    return letter in ['a', 'b', 'c', 'd'] and 0 < count <= 64


def parse_data_with_counts(file_path):
    data_dict = {'a': [], 'b': [], 'c': [], 'd': []}  # Initialize a dictionary with a list for 'd'.

    with open(file_path, "rb") as file:
        header_number = 0
        # ff = file.read(2)
        #print(ff)
        while True:
            # Read header which consists of the letter and the count byte.
            header = file.read(2)
            # print(header)
            # return
            if not header:
                break  # End of file reached

            try:
                # Decode the letter and get the count directly
                letter, count = struct.unpack('cB', header)
                letter = letter.decode('utf-8')
            except (struct.error, UnicodeDecodeError) as e:
                print(f"Error unpacking header: {e}")
                continue

            # Check if the header is valid
            if letter == 'd' and count > 0:
                channels = ['a', 'b', 'c', 'd']
                for chn in channels:
                    integers = []
                    for _ in range(count):
                        data_bytes = file.read(2)  # Read 2 bytes per integer
                        if not data_bytes:
                            break  # End of file or data corruption
                        try:
                            integer = struct.unpack('<h', data_bytes)[0]
                            integers.append(integer)
                        except struct.error as e:
                            print(f"Error unpacking data: {e}")
                            break  # Stop processing this packet

                    # Append the integers to the list for 'd' if any were read
                    if integers:
                        data_dict[chn].extend(integers)
            t = file.read(6)

    return data_dict

# Replace 'your_file_path.bin' with the actual path of your binary file.
data_dict = parse_data_with_counts('../../measurement_data/data_amp/test_bias_3300.dat')
for letter, values in data_dict.items():
    print(f"{letter} len", len(values))


num_letters = len(data_dict)
cols = 2
rows = num_letters // cols + (num_letters % cols > 0)

plt.figure(figsize=(10, 6 * rows))  # Adjust the figure size accordingly

for i, (letter, values) in enumerate(data_dict.items(), start=1):
    ax = plt.subplot(rows, cols, i)  # Create a subplot for each histogram

    # Calculate the bin edges so each integer value has its own bin
    min_value, max_value = min(values), max(values)
    bins = range(min_value, max_value + 2)  # +2 to include the right edge of the last bin
    print(letter)
    ax.hist(values, bins=bins, alpha=0.75, align='left')  # Align bins to the left of the values
    ax.set_title(f"Histogram for Letter '{letter}'")
    ax.set_xlabel("Value")
    ax.set_ylabel("Frequency")
    ax.set_xticks(range(min_value, max_value + 1))  # Ensure x-ticks cover all integer values
    # ax.grid(True)
plt.tight_layout()  # Adjust subplots to fit into the figure area.
plt.show()


