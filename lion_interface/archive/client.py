import socket
import time


def receive_and_write_to_file(server_ip, server_port, filename, buffer_size=264):
    # Buffer to temporarily hold data
    buffer = bytearray()

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((server_ip, server_port))
        once = True
        with open(filename, 'wb') as file:
            data_length = 0
            first = True
            while True:
                data = s.recv(64)  # Adjust based on the expected data size
                if first:
                    t1 = time.time()
                    first = False
                # print(data)
                if not data:
                    break  # Exit i                            f no more data

                # Optionally convert bytes to integer here if needed
                # integer = int.from_bytes(data, byteorder='big')
                # For simplicity, we're directly buffering the bytes

                buffer.extend(data)

                # If buffer reaches the specified size, write to file
                if len(buffer) >= buffer_size:
                    data_length += len(buffer)
                    t2 = time.time()
                    print(data_length, data_length / (t2 - t1))
                    if data_length >= 20000 and once:
                        once = False
                        # cmd = bytes('!', encoding='utf-8')
                        # print("send", cmd)
                        # s.send(cmd)
                    file.write(buffer)
                    buffer.clear()  # Clear buffer after writing

            # Write any remaining data in buffer to file
            if buffer:
                file.write(buffer)


# Example usage
#server_ip = '192.168.1.200'  # Replace with your server's IP
server_ip = '10.42.0.244'  # Replace with your server's IP

server_port = 10001  # Replace with your server's port
filename = 'output.dat'  # Output file name
receive_and_write_to_file(server_ip, server_port, filename)
