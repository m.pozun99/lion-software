import socket
import struct
import random
import time
def start_test_server(host, port, mean, std_dev, num_letters=10):
    """
    Start a TCP server that sends 4 data points for letters 'a', 'b', 'c', and 'd'.
    Each data point is generated around a Gaussian distribution.

    :param host: Server host IP
    :param port: Server port
    :param mean: Mean value for the Gaussian distribution
    :param std_dev: Standard deviation for the Gaussian distribution
    :param num_letters: Number of packets to send for each letter
    """
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
        server_socket.bind((host, port))
        server_socket.listen()
        print(f"Server listening on {host}:{port}")

        conn, addr = server_socket.accept()
        with conn:
            print(f"Connected by {addr}")

            for _ in range(num_letters):
                for letter in ['a', 'b', 'c', 'd']:
                    data_packet = struct.pack('cB', letter.encode(), 4)  # Letter and count
                    for _ in range(4):  # Now sending 4 data points
                        value = int(random.gauss(mean, std_dev))
                        # Ensure the value fits in 2 bytes.
                        value = max(min(value, 32767), -32768)
                        data_packet += struct.pack('<h', value)

                    conn.sendall(data_packet)
                    time.sleep(0.01)

            print("Data has been sent.")

if __name__ == "__main__":
    host = '127.0.0.1'  # Localhost
    port = 10001  # Example port
    mean = 100  # Example mean value for Gaussian distribution
    std_dev = 15  # Example standard deviation
    num = 1000
    start_test_server(host, port, mean, std_dev, num)
