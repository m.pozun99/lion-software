import struct

from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QHBoxLayout
from PyQt5.QtCore import QTimer  # For alignment and other constants
import sys
import numpy as np

from desktop_app.lion_gui.measurement_widget import MeasWidget
from fit_functions.fit_manager import FitRatioManager
from desktop_app.lion_gui.chart import MplCanvas
import datetime

from desktop_app.lion_gui.settings_widget import LionSettingsWidget
from fit_functions.solution import SolutionManager
from desktop_app.tcp_client import LionClient


class LionWindow(QMainWindow):
    def __init__(self, client, solution_manager):
        super(LionWindow, self).__init__()
        self.setWindowTitle("LION CONTROL")
        self.setGeometry(100, 100, 1000, 1000)

        self.client = client
        self.solution_manager = solution_manager

        # Create the title label
        # self.title_label = QLabel("Measurement Overview")
        # self.title_label.setAlignment(Qt.AlignCenter)  # Center-align the title
        # layout.addWidget(self.title_label)

        self.canvas = MplCanvas(self, "ION BEAM POSITION")
        self.canvas.setFixedSize(800, 800)
        self.settings_widget = LionSettingsWidget(self.client)
        self.meas_widget = MeasWidget(self.client, self)

        self.client.add_data_callback(self.process_data)


        self._create_layout()
        # Generate some large random data
        # x = np.random.normal(0, 5, 1000000)
        # y = np.random.normal(0, 10, 1000000)
        # self.canvas.update_plot(x, y)

        # self.client.connect_to_server()
        # self.settings_widget.get_settings_from_client()

        self.data_buffer = bytearray()  # Initialize an empty byte array to store incoming data
        self.measuring = False
        self.meas_counts = 0
        self.start_time = None
        self.stop_time = None
        self.temperature = None

        self.expected_length = 2 + (32 * 2 * 4) + 4 + 2
        self.update_timer = QTimer(self)
        self.update_timer.timeout.connect(self.update_measurement_display)
        self.update_timer.start(5000)  # Timer interval is set in milliseconds



    def process_data(self, new_data, length=32):
        # Append new data to the buffer
        self.data_buffer += new_data

        # Check if we have enough data to process
        while len(self.data_buffer) >= self.expected_length:
            self.meas_counts += length
            print("process")
            # Calculate the number of bytes needed per channel (16 bits per integer)
            data_length = length * 2  # each short is 2 bytes, and we need length shorts

            # Calculate offsets for each channel
            offset_a = 2
            offset_b = offset_a + data_length
            offset_c = offset_b + data_length
            offset_d = offset_c + data_length
            offset_temperature = 256

            # Safely unpack data for each channel if enough data is available
            integers_a = struct.unpack('<' + str(length) + 'h', self.data_buffer[offset_a:offset_a + data_length])
            integers_b = struct.unpack('<' + str(length) + 'h', self.data_buffer[offset_b:offset_b + data_length])
            integers_c = struct.unpack('<' + str(length) + 'h', self.data_buffer[offset_c:offset_c + data_length])
            integers_d = struct.unpack('<' + str(length) + 'h', self.data_buffer[offset_d:offset_d + data_length])
            # Calculate ratios (ensure division by zero is handled if needed)
            integers_a = np.array(integers_a)
            integers_b = np.array(integers_b)
            integers_c = np.array(integers_c)
            integers_d = np.array(integers_d)
            ratio_x = np.divide(integers_b - integers_d, integers_b + integers_d, where=integers_d != 0)
            ratio_y = np.divide(integers_c - integers_a, integers_c + integers_a, where=integers_c != 0)
            # Assuming `solve_ratios` function handles numpy arrays and `self.solution_manager` and `self.canvas` are properly initialized
            x, y = self.solution_manager.solve_ratios(ratio_x, ratio_y, table=True)
            self.temeprature = struct.unpack('<f', self.data_buffer[offset_temperature:offset_temperature + 4])
            self.temperature = 29.5
            print("temperature", self.temperature)
            self.canvas.add_data(x, y)

            # Remove processed data from buffer
            self.data_buffer = self.data_buffer[self.expected_length:]




    def update_info_label(self, x, y):
        start_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        end_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        total_counts = len(x)  # Assuming the number of counts is the length of x
        counts_per_second = total_counts / (10)  # Assuming 10 seconds for example

        self.info_label.setText(f"Start Time: {start_time}\nEnd Time: {end_time}\nTotal Counts: {total_counts}\nCounts per Second: {counts_per_second}")

    def start_client_measurement(self, filename=None):
        self.meas_counts = 0
        self.measuring = True
        self.start_time = datetime.datetime.now()
        self.client.start_measurement(filename=filename)
        for j in range(10):
            with open("../../measurement_data/data_lion_new/pos_0_-15_data.dat", "rb") as f:
                data = f.read()
            for i in range(0, len(data), self.expected_length):
                self.process_data(data[i: i + self.expected_length])

    def stop_client_measurement(self):
        self.measuring = False
        self.stop_time = datetime.datetime.now()
        self.canvas.reset()
        self.client.stop_measurement()

    def update_measurement_display(self):
        if self.measuring:
            self.canvas.update_plot_self()
            time = datetime.datetime.now() -  self.start_time
            time_seconds = time.total_seconds()
            rate = self.meas_counts / time_seconds
            meas_info = {"counts": self.meas_counts, "time": time_seconds, "rate": rate,  "temperature": self.temperature}
            self.meas_widget.upate_meas_info(meas_info)


    def _create_layout(self):
        main_lay = QHBoxLayout()
        left_lay = QVBoxLayout()
        right_lay = QVBoxLayout()
        main_lay.addLayout(left_lay)
        main_lay.addLayout(right_lay)

        # left lay
        # meas layout

        # canvas
        # self.title_label = QLabel("ION BEAM POSITION")
        # self.title_label.setStyleSheet(title_style)
        # left_lay.addWidget(self.title_label)
        # layout.addWidget(self.info_label)
        left_lay.addWidget(self.canvas)

        # left_lay.addLayout(meas_lay)

        # right lay
        right_lay.addWidget(self.settings_widget)
        right_lay.addWidget(self.meas_widget)

        widget = QWidget()
        widget.setLayout(main_lay)
        self.setCentralWidget(widget)

    def closeEvent(self, *args, **kwargs):
        super().closeEvent(*args, **kwargs)
        self.client.stop_measurement()




if __name__ == "__main__":
    app = QApplication(sys.argv)
    server_ip = '192.168.1.200'  # Replace with your server's IP
    # server_ip = '10.42.0.244'  # Replace with your server's IP
    server_port = 10001  # Replace with your server's port
    filename = 'settings.dat'  # Example filename

    x_fit_manager = FitRatioManager.get_from_file('../archive/x_fit_manager_new.pkl')
    y_fit_manager = FitRatioManager.get_from_file('../archive/y_fit_manager_new.pkl')
    sol_manager = SolutionManager.load_grid_from_file(1000, "../archive/solution_manager_lion.pkl")
    sol_manager.add_fit_x(x_fit_manager)
    sol_manager.add_fit_y(y_fit_manager)
    client = LionClient(server_ip, server_port, filename)
    window = LionWindow(client, sol_manager)
    window.show()
    sys.exit(app.exec_())
