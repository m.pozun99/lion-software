import numpy as np
from collections import deque
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

class MplCanvas(FigureCanvas):
    EDGE_LENGTH = 70
    BINS = 140
    MAX_POINTS = 1000000

    def __init__(self, parent=None, title=None, width=5, height=5, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        self.axes.set_position([0.2, 0.2, 0.7, 0.7])
        self.ax_histx = fig.add_axes([0.2, 0.1, 0.7, 0.1], sharex=self.axes)
        self.ax_histy = fig.add_axes([0.1, 0.2, 0.1, 0.7], sharey=self.axes)
        self.ax_histx.set_yticklabels([])
        self.ax_histy.set_xticklabels([])
        self.ax_histx.tick_params(axis='y', which='both', left=False, right=False, labelleft=False)
        self.ax_histy.tick_params(axis='x', which='both', top=False, bottom=False, labelbottom=False)


        # Use deque for x and y data to efficiently handle large data sets
        self.x_data = deque(maxlen=self.MAX_POINTS)
        self.y_data = deque(maxlen=self.MAX_POINTS)
        self.title = title

        super(MplCanvas, self).__init__(fig)
        self.setParent(parent)

    def add_data(self, x, y):
        """Add data points to the internal storage and automatically update the plot if max points are reached."""
        print("Data addeddddddd")
        self.x_data.extend(x)
        self.y_data.extend(y)

    def update_plot(self, x, y):
        heatmap, xedges, yedges = np.histogram2d(x, y, bins=self.BINS,
                                                 range=[[-self.EDGE_LENGTH, self.EDGE_LENGTH],
                                                        [-self.EDGE_LENGTH, self.EDGE_LENGTH]])
        self.axes.cla()
        self.ax_histx.cla()
        self.ax_histy.cla()
        extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
        self.axes.imshow(heatmap.T, extent=extent, origin='lower', cmap='inferno')
        self.axes.set_xlim(xedges[0], xedges[-1])
        self.axes.set_ylim(yedges[0], yedges[-1])
        if self.title:
            self.axes.set_title(self.title)
        self.ax_histx.hist(x, bins=xedges, color='gray', alpha=0.7)
        self.ax_histy.hist(y, bins=yedges, orientation='horizontal', color='gray', alpha=0.7)
        self.axes.set_aspect('equal')
        self.ax_histx.set_xlabel('$x [mm]$')
        self.ax_histy.set_ylabel('$y [mm]$')
        self.draw()

    def update_plot_self(self):
        try:
            self.update_plot(np.array(self.x_data), np.array(self.y_data))
        except ValueError as e:
            print(e)

    def reset(self):
        self.x_data = deque(maxlen=self.MAX_POINTS)
        self.y_data = deque(maxlen=self.MAX_POINTS)
        self.update_plot_self()


    def draw_pos_specifiers(self, x, y):
        self.axes.axvline(x=x, color='red', linewidth=0.5)
        self.axes.axhline(y=y, color='red', linewidth=0.5)
        self.draw()
