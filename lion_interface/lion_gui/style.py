# QT style sheets
title_style = "QLabel { font-family: 'Arial'; font-size: 16pt; color: black; padding-bottom: 10px;}"
title_style_active = "QLabel { font-family: 'Arial'; font-size: 14pt; color: green; padding-bottom: 10px;}"
title_style_not_active = "QLabel { font-family: 'Arial'; font-size: 14pt; color: red; padding-bottom: 10px;}"
section_title_style = "QLabel { font-family: 'Arial'; font-size: 14pt; color: black; padding-top: 10px;padding-bottom: 10px;}"
normal_style = "QLabel { font-family: 'Arial'; font-size: 12pt; }"
quantity_label = "QLabel { font-family: 'Arial'; font-size: 10pt; }"
bgo_widget_style = """
#BGOwidget {
    border: 2px solid black;
    border-radius: 5px;  /* if you want rounded corners */
}
"""

