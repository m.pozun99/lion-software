"""
This module includes main window of hardware GUI package.
"""
import time

from PyQt5.QtCore import QEvent, Qt
from typing import Dict, Tuple

from PyQt5.QtWidgets import QLabel, QLayout, QSpinBox, QDoubleSpinBox, QHBoxLayout, QVBoxLayout, QSizePolicy, QFrame, \
    QPushButton, QWidget, QLineEdit, QGridLayout

from desktop_app.lion_gui.style import  *


class QChar:
    pass


class MeasWidget(QWidget):
    """Widget measurement.

    Set and get parameters for BGO

    """
    MEAS_FOLDER = "data_lion/"
    LABEL_SIZE_POLICY = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)

    def __init__(self, client, mw) -> None:
        super().__init__()
        self.client = client
        self.mw = mw
        # -------MPL AGENTS--------------
        self._create_layout()
        self.set_button_connections()

    def _create_layout(self) -> None:
        """GUI elements are created."""
        # ------ ---------------------
        main_lay = QVBoxLayout()
        title = f"MEASUREMENT"
        self.title_label = QLabel(title)
        self.title_label.setStyleSheet(title_style)
        self.title_label.setSizePolicy(self.LABEL_SIZE_POLICY)

        main_lay.addWidget(self.title_label)
        # ------ SETTINGS MENU----
        # self.label_filename = QLabel("Filename")
        # self.label_filename.setAlignment(Qt.AlignCenter)
        # self.label_filename.setSizePolicy(self.LABEL_SIZE_POLICY)
        # lay.addWidget(self.label_filename)
        # self.edit_filename = QLineEdit()
        # lay.addWidget(self.edit_filename)
        meas_lay = QVBoxLayout()
        # self.meas_label = QLabel("MEASUREMENT INFO")
        # self.meas_label.setStyleSheet(title_style)
        self.count_label = QLabel("Counts: ")
        self.count_label.setStyleSheet(quantity_label)
        self.time_meas_label = QLabel("Time: ")
        self.time_meas_label.setStyleSheet(quantity_label)
        self.rate_label = QLabel("Rate: ")
        self.rate_label.setStyleSheet(quantity_label)
        self.temp_label = QLabel("Temperature: ")
        self.temp_label.setStyleSheet(quantity_label)

        # meas_lay.addWidget(self.meas_label)
        meas_lay.addWidget(self.count_label)
        meas_lay.addWidget(self.time_meas_label)
        meas_lay.addWidget(self.rate_label)
        meas_lay.addWidget(self.temp_label)
        main_lay.addLayout(meas_lay)

        lay = QVBoxLayout()
        self.btn_start_measurement = QPushButton("Start Measurement")
        self.btn_stop_measurement = QPushButton("Stop Measurement")
        self.btn_stop_measurement.setEnabled(True)
        lay.addWidget(self.btn_start_measurement)
        lay.addWidget(self.btn_stop_measurement )
        main_lay.addLayout(lay)


        main_lay.addStretch(0)
        self.setLayout(main_lay)
        self.setStyleSheet(bgo_widget_style)

    def upate_meas_info(self, meas_info):
        meas_time = meas_info["time"]
        meas_counts = meas_info["counts"]
        meas_rate = meas_info["rate"]
        temperature = meas_info["temperature"]

        self.time_meas_label.setText(f"Time: {meas_time:.1f} seconds")
        self.count_label.setText(f"Counts: {meas_counts}")
        self.rate_label.setText(f"Rate: {meas_rate:.1f} Counts / second")
        self.temp_label.setText(f"Temperature: {temperature:.1f}°C")



    def set_button_connections(self) -> None:
        """ Connects widgets to functions"""
        self.btn_start_measurement.clicked.connect(self.start_measurement)
        self.btn_stop_measurement.clicked.connect(self.stop_measurement)



    # ----------------------------------------------------
    def get_filename(self):
        return None
        # text = self.MEAS_FOLDER + self.edit_filename.text() + "_data.dat"
        # return text
    def start_measurement(self):
        self.mw.start_client_measurement(filename=self.get_filename())
        self.btn_start_measurement.setEnabled(False)
        self.btn_stop_measurement.setEnabled(True)


    def stop_measurement(self):
        self.client.stop_measurement()
        self.btn_start_measurement.setEnabled(True)
        self.btn_stop_measurement.setEnabled(False)



    def closeEvent(self, event: QEvent) -> None:
        event.accept()