"""
This module includes main window of hardware GUI package.
"""

from PyQt5.QtCore import QEvent, Qt
from typing import Dict, Tuple

from PyQt5.QtWidgets import QLabel, QLayout, QSpinBox, QDoubleSpinBox, QHBoxLayout, QVBoxLayout, QSizePolicy, QFrame, \
    QPushButton, QWidget

from desktop_app.lion_gui.style import  *


#
# from desktop_app.web_api.bgo_interface import I2CControllerClient


class LionSettingsWidget(QWidget):
    """Widget for one BGO.

    Set and get parameters for BGO

    """
    COLOR_SETTING_APPLIED = "white"
    COLOR_SETTING_NOT_APPLIED = "lightblue"
    LABEL_SIZE_POLICY = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)

    def __init__(self, client) -> None:
        super().__init__()
        self.client = client
        self.saved_settings = dict(Vbias=None, Vthresh=None)
        #self.saved_data = dict(Ileak=None, temp=None)
        #self.timer_period = None
        # ------SPIN WIDGETS-------------
        self.hw_spin = {}
        self.hw_spin_real = {}
        # -------------------------------
        #self.meas_labels = {}

        # -------MPL AGENTS--------------
        self._create_layout()
        self.set_button_connections()
        self.empty_settings = self.get_settings_from_GUI()
        self.saved_settings = self.get_settings_from_GUI()

    def _create_layout(self) -> None:
        """GUI elements are created."""
        # ------ ---------------------
        main_lay = QVBoxLayout()
        title = f"SETTINGS"
        self.title_label = QLabel(title)
        self.title_label.setStyleSheet(title_style)
        self.title_label.setSizePolicy(self.LABEL_SIZE_POLICY)

        main_lay.addWidget(self.title_label)
        lay = QVBoxLayout()
        # ------ SETTINGS MENU----
        self.label_DAC = QLabel("DAC")
        self.label_DAC.setStyleSheet(section_title_style)
        self.label_DAC.setAlignment(Qt.AlignCenter)
        self.label_DAC.setSizePolicy(self.LABEL_SIZE_POLICY)
        lay.addWidget(self.label_DAC)
        self.add_DAC_spin_row("V<sub>bias</sub>", lay, ran=self.client.DAC_RANGE, tag="Vbias")
        self.add_DAC_spin_row("V<sub>threshold</sub>", lay, ran=self.client.DAC_RANGE, tag="Vthresh")
        btn_lay = QHBoxLayout()
        self.btn_apply_settings = QPushButton("Apply settings")
        self.btn_discard_settings = QPushButton("Discard settings")
        btn_lay.addWidget(self.btn_apply_settings)
        btn_lay.addWidget(self.btn_discard_settings)
        lay.addLayout(btn_lay)

        main_lay.addLayout(lay)
        main_lay.addStretch(0)
        self.setLayout(main_lay)
        self.setStyleSheet(bgo_widget_style)

    def set_button_connections(self) -> None:
        """ Connects widgets to functions"""
        self.hw_spin["Vbias"].valueChanged.connect(self.vbias_setting_changed)
        self.hw_spin_real["Vbias"].valueChanged.connect(self.vbias_real_value_changed)
        self.hw_spin["Vthresh"].valueChanged.connect(self.vthresh_setting_changed)
        self.hw_spin_real["Vthresh"].valueChanged.connect(self.vthresh_real_value_changed)
        self.btn_apply_settings.clicked.connect(self.apply_settings)
        self.btn_discard_settings.clicked.connect(self.discard_settings_changes)

    def get_settings_from_GUI(self) -> Dict[str, float]:
        settings = dict()
        settings["Vbias"] = self.hw_spin["Vbias"].value()
        settings["Vthresh"] = self.hw_spin["Vthresh"].value()
        return settings

    def set_settings_to_GUI(self, settings: Dict[str, float]) -> None:
        print(settings)
        for key in self.hw_spin.keys():
            self.hw_spin[key].setValue(settings[key])
        self.save_settings(settings)

    # def set_data_to_GUI(self, data: Dict[str, float]) -> None:
    #     ileak_info = f"{data['Ileak']:.2f} uA"
    #     self.meas_labels["Ileak"].setText(ileak_info)
    #     temp_info = f"{data['temp']:.2f} °C "
    #     self.meas_labels["temp"].setText(temp_info)
    #     self.save_data(data)

    def apply_settings(self) -> None:
        """Compares settings in GUI widgets and if they differ to previously applied settings than they are applied"""

        settings_gui = self.get_settings_from_GUI()
        if not self.compare_settings(settings_gui):
            self.save_settings(settings_gui)
            self.send_settings_to_client(self.saved_settings)

    def discard_settings_changes(self) -> None:
        """Changes made in widgets are discarded and replaced with currently applied values"""

        self.set_settings_to_GUI(self.saved_settings)
        for key in self.hw_spin.keys():
            self.color_spin_box(key)

    def save_settings(self, settings: Dict[str, float]):
        self.saved_settings = settings
        for key in self.hw_spin.keys():
            self.color_spin_box(key)

    def save_data(self, data: Dict[str, float]):
        self.saved_data = data

    def compare_settings(self, settings: Dict[str, float]) -> bool:
        for key, value in settings.items():
            if value != self.saved_settings[key]:
                return False
        return True

    # ------------------------------------------

    # ------WIDGETS COLORING AND SETUP-------

    def color_spin_box(self, tag: str) -> bool:
        """Spin widget is colored based on comparison of a widget value and a saved value.

        Args:
            tag: Key of spin box in hw_spin class dictionary.

        Returns:
             bool: If values match True is returned, else False
        """
        saved_value = self.saved_settings[tag]
        if self.hw_spin[tag].value() == saved_value:
            self.hw_spin[tag].setStyleSheet("QSpinBox""{""background-color : " + self.COLOR_SETTING_APPLIED + ";""}")
            return True
        else:
            self.hw_spin[tag].setStyleSheet(
                "QSpinBox""{""background-color : " + self.COLOR_SETTING_NOT_APPLIED + ";""}")
            return False

    def add_DAC_spin_row(self, label: str, layout: QLayout, ran: Tuple[int, int] = None, tag: str = None) -> None:
        """ Create row in menu with additional widget for real value of parameter.

        Function creates a row with QLabel, QSpinBox and QDoubleSpinBox that is added to the layout.
        QDoubleSpinBox represents a calculated value of QSpinBox. Spin box is added to
        class dictionary hw_spin and double spin box is added to hw_spin_calc.

        Args:
            label:  Name of the setting displayed in GUI
            layout: Layout to which row will be added.
            ran: Range of spin box values.
            tag: Key of a selected parameter in "hw_spin" dictionary.
        """

        if tag is None:
            tag = label
        lab = QLabel(label)
        lab.setStyleSheet(quantity_label)
        spin_box = QSpinBox()
        double_spin_box = QDoubleSpinBox()
        self.hw_spin[tag] = spin_box
        self.hw_spin_real[tag] = double_spin_box
        spin_box.setRange(ran[0], ran[1])
        double_spin_box.setDecimals(3)
        double_spin_box.setSuffix("V")
        lay = QHBoxLayout()
        lay.addWidget(lab)
        lay.addWidget(spin_box)
        lay.addWidget(double_spin_box)
        layout.addLayout(lay)

    def add_meas_row(self, label: str, layout: QLayout, tag: str = None) -> None:
        if tag is None:
            tag = label
        lab = QLabel(label)
        lab.setStyleSheet(quantity_label)
        meas_label = QLabel()
        self.meas_labels[tag] = meas_label
        lay = QHBoxLayout()
        lay.addWidget(lab)
        lay.addWidget(meas_label)
        layout.addLayout(lay)

    # ------------------------------------------

    # ------PARAMETER CHANGED FUNCTIONS---------
    '''Those functions are responsible for handling user input of settings'''

    def vbias_setting_changed(self) -> None:
        dac_setting = self.hw_spin["Vbias"].value()
        real_value = self.client.calc_vbias_value_from_setting(dac_setting)
        self.hw_spin_real["Vbias"].setValue(real_value)
        self.color_spin_box("Vbias")

    def vbias_real_value_changed(self) -> None:
        real_value = self.hw_spin_real["Vbias"].value()
        dac_setting = self.client.calc_vbias_setting_from_value(real_value)
        self.hw_spin["Vbias"].setValue(dac_setting)
        self.color_spin_box("Vbias")

    def vthresh_setting_changed(self) -> None:
        dac_setting = self.hw_spin["Vthresh"].value()
        real_value = self.client.calc_vthresh_value_from_setting(dac_setting)
        self.hw_spin_real["Vthresh"].setValue(real_value)
        self.color_spin_box("Vthresh")

    def vthresh_real_value_changed(self) -> None:
        real_value = self.hw_spin_real["Vthresh"].value()
        dac_setting = self.client.calc_vthresh_setting_from_value(real_value)
        self.hw_spin["Vthresh"].setValue(dac_setting)
        self.color_spin_box("Vthresh")

    def settings_enabled(self, enabled):
        self.hw_spin["Vthresh"].setEnabled(enabled)
        self.hw_spin_real["Vthresh"].setEnabled(enabled)
        self.hw_spin["Vbias"].setEnabled(enabled)
        self.hw_spin_real["Vbias"].setEnabled(enabled)


    def clear_gui(self):
        self.set_settings_to_GUI(self.empty_settings)

    def set_active(self, active):
        if active:
            self.title_label.setStyleSheet(title_style_active)
            self.settings_enabled(True)
        else:
            self.title_label.setStyleSheet(title_style_not_active)
            self.settings_enabled(False)


    # ----------------------------------------------------

    # ------------------------------------------------------

    # -----COMMUNICATION WITH CLIENT--------------------
    # ------------------INPUT--------------------------------
    def send_settings_to_client(self, settings):
        self.client.set_vbias_setting(settings["Vbias"])
        self.client.set_vthresh_setting(settings["Vthresh"])

    def get_settings_from_client(self):
        settings = dict()
        settings["Vbias"] = self.client.get_vbias_setting()
        settings["Vthresh"] = self.client.get_vthresh_setting()
        self.set_settings_to_GUI(settings)

    def get_data_from_client(self):
        data = dict()
        data["Ileak"] = self.client.get_ileak(self.address, convert=True)
        data["temp"] = self.client.get_temp(self.address, convert=True)
        counts = self.client.get_counts(self.address)
        if self.timer_period is not None and counts is not None:
            data["rate"] = counts / self.timer_period * 1000
        else:
            data["rate"] = None
        if any(value is None for value in data.values()):
            self.set_active(False)
        else:
            self.set_active(True)
            # self.set_data_to_GUI(data)
        return data

    def save_settings_to_flash(self):
        self.client.save_settings_to_flash(self.address)

    def apply_settings_from_flash(self):
        self.client.apply_settings_from_flash(self.address)

    def closeEvent(self, event: QEvent) -> None:
        event.accept()