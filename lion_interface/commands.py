import struct


def generate_command(cmd_group, cmd_id, extra_bytes=0):
    cmd = bytearray()
    cmd += struct.pack("<H", extra_bytes)
    cmd += bytearray([cmd_group, cmd_id])
    return cmd


ANALOG_GROUP = 0
DIGITAL_GROUP = 1
MEAS_GROUP = 2
MISC_GROUP = 3


# DAC
GET_VBIAS = generate_command(ANALOG_GROUP, 0)
SET_VBIAS = generate_command(ANALOG_GROUP, 1, 2)
GET_VTHRESH = generate_command(ANALOG_GROUP, 2)
SET_VTHRESH = generate_command(ANALOG_GROUP, 3, 2)
START_MEASUREMENT = generate_command(ANALOG_GROUP, 4)
STOP_MEASUREMENT = bytes('!', encoding='utf-8')
#STOP_MEAS = generate_command(ANALOG_GROUP, 5)



# DIGITAL
SET_DISCHARGE_TIMER_PERIOD = generate_command(DIGITAL_GROUP, 0, 4)
SET_DISCHARGE_TIMER_PWM= generate_command(DIGITAL_GROUP, 1, 4)
SET_TRIGGER_TIMER_PERIOD = generate_command(DIGITAL_GROUP, 2, 4)
SET_TRIGGER_TIMER_PRESCALER = generate_command(DIGITAL_GROUP, 3, 4)


# FLASH
SAVE_SETTINGS_FLASH = generate_command(MISC_GROUP, 0)
GET_SETTINGS_FLASH = generate_command(MISC_GROUP, 1)
GET_TEMPERATURE = generate_command(MISC_GROUP, 2)


if __name__ == "__main__":
    print(GET_VBIAS)