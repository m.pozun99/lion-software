from fit_functions.fit_manager import FitRatioManager
import numpy as np
from measurement.measurement_group import MeasurementGroup

from fit_functions.solution import SolutionManager

FOLDER = "desktop_app/data_lion_new/"
DATA_FOLDER = FOLDER
# offsets = {"Channel A": 1, "Channel B": 1.2, "Channel C": 1.2, "Channel D": 2.2}
offsets = {"Channel A": 0, "Channel B": 0, "Channel C": 0, "Channel D": 0}

NUMBER_OF_MEASUREMENTS = 10000

mgroup = MeasurementGroup(folder=FOLDER, offsets=offsets, data_folder=DATA_FOLDER, binary=True)
x_values, y_values = mgroup.get_coordinate_arrays()
x_values = np.array(x_values)
y_values = np.array(y_values)
ratio_x, ratio_y = mgroup.get_ratios_normed()
ratio_x_orig, ratio_y_orig = mgroup.get_ratios()
ratio_x_std, ratio_y_std = mgroup.get_ratios_std_normed()

ratio_x = np.array(ratio_x)
ratio_y = np.array(ratio_y)
print(x_values)
print(y_values)
# =============================================================
init_params = {'a': 1, 'd0': 100, "l": 100, "y01": 0, "y02": 0, "x0": 0}
fit_manager_x_angle = FitRatioManager("angle", init_params, x_values, y_values)
fit_manager_x_angle.fit(ratio_x)
print(fit_manager_x_angle.result.fit_report())
fit_manager_x_angle.plot_ratios(ratio_x, "ratio x angle", style="--", clear=False, show=True, filename="plots/x_ratio.pgf", x=True)

init_params = {'a': 1, 'd0': 100, "y01": 0, "y02": 0}
fit_manager_x = FitRatioManager("1/x", init_params, x_values, y_values)
fit_manager_x .fit(ratio_x)
print(fit_manager_x .result.fit_report())
#fit_manager_x .plot_ratios(ratio_x, "ratio x", style="-", clear=False)


# ==================================================== y
init_params = {'a': 1, 'd0': 100, "l": 100, "y01": 0, "y02": 0, "x0":0}
fit_manager_y_angle = FitRatioManager("angle", init_params, y_values, x_values)
fit_manager_y_angle.fit(ratio_y)
print(fit_manager_y_angle.result.fit_report())
fit_manager_y_angle.plot_ratios(ratio_y, "ratio y angle", style="--", clear=True, show=True, filename="plots/y_ratio.pgf", x=False)

# ========================================================= #
init_params = {'a': 1, 'd0': 100, "y01": 0, "y02": 0}
fit_manager_y = FitRatioManager("1/x", init_params, y_values, x_values)
fit_manager_y.fit(ratio_y)
print(fit_manager_y.result.fit_report())
# fit_manager_y.plot_ratios(ratio_y, "ratio y", style="-", clear=False)

fit_manager_x_angle.save('x_fit_manager_new.pkl')
fit_manager_y_angle.save('y_fit_manager_new.pkl')

print("ANGLE****************************************")
sol2 = SolutionManager()
sol2.add_fit_x(fit_manager_x_angle)
sol2.add_fit_y(fit_manager_y_angle)

for i in range(len(ratio_x_orig)):
    x_ratio = ratio_x[i]
    y_ratio = ratio_y[i]
    x_std = ratio_x_std[i]
    y_std = ratio_y_std[i]
    x = x_values[i]
    y = y_values[i]
    estimate = sol2.solve_single_ratio(x_ratio, y_ratio)
    estimate_std = sol2.estimate_unc(x_ratio, x_std, y_ratio, y_std)

    # print(f"RATIO X: {x_ratio:8.2f}   STDX_X: {x_std:8.3f}     RATIO Y: {y_ratio:8.3f}  STDX_Y: {y_std:8.4f} ")
    print(f"COORDINATE: {x:6.1f}, {y:6.1f}   ESTIMATED: {estimate[0]:6.1f} ±{estimate_std[0]:6.1f}, {estimate[1]:5.1f} ±{estimate_std[1]:6.1f}   DIFF: {x - estimate[0]:6.1f}, {y - estimate[1]:6.1f}")


# folder = "desktop_app/data_lion/"
# meas = MeasurementObject("pos_-30_45", folder, 10000)
# meas.get_data_from_binary()
# meas.plot_all_channel_histograms( show=True)