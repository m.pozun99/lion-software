import pickle

from measurement.measurement import MeasurementObject
import numpy as np
import os

def get_subdirectories(base_path):
    """
    Returns the names of all subdirectories from the specified folder as a list.

    :param base_path: Path of the base directory
    :return: List of subdirectory paths
    """
    subdirectories = []
    for root, dirs, _ in os.walk(base_path):
        for name in dirs:
            subdirectories.append(name)

    return subdirectories

def get_filenames(base_path):
    """
    Returns the names of all files from the specified folder as a list.

    :param base_path: Path of the base directory
    :return: List of file paths
    """
    names = []
    for root, _, files in os.walk(base_path):
        for name in files:
            names.append(name)
    return names

class MeasurementGroup():
    def __init__(self, offsets, folder=None, data_folder="data_calibration/", binary=False):
        self.folder = folder
        self.offsets = offsets

        self.names = None
        self.measurements = None
        if self.folder is not None:
            if not binary:
                self.names = get_subdirectories(self.folder)
                self.get_all_data_from_folder(self.folder, data_folder, self.offsets)
            else:
                self.get_all_data_from_folder_binary(self.folder, data_folder, self.offsets)


    def get_all_data_from_folder_binary(self, FOLDER, DATA_FOLDER, offsets=None):
        names = get_filenames(self.folder)
        self.ratios = dict()
        self.channel_values = dict()
        self.measurements = dict()
        for name in names:
            # splt = name.split("_")
            # x = int(splt[1])
            # y = int(splt[2])
            # if abs(x) != 0 and abs(y) != 0 and abs(x) != 15 and abs(y) != 15:
            #     continue
            data_folder = DATA_FOLDER
            meas = MeasurementObject(name, FOLDER)
            meas.get_data_from_binary(data_folder)
            if offsets is not None:
                meas.apply_offsets(offsets)
            meas.calculate_ratios()
            self.measurements[name] = meas
            self.ratios[name] = meas.get_ratios()
            self.channel_values[name] = meas.get_mean_values_and_stds()


    def get_all_data_from_folder(self, FOLDER, DATA_FOLDER, offsets=None):
        self.names = get_subdirectories(FOLDER)
        self.ratios = dict()
        self.channel_values = dict()
        self.measurements = dict()
        for name in self.names:
            data_folder = DATA_FOLDER
            meas = MeasurementObject(name, FOLDER)
            try:
                meas.get_data_from_files(data_folder)
            except:
                meas.get_data_from_csv(name)
                meas.save_data_to_files(data_folder)

            if offsets is not None:
                meas.apply_offsets(offsets)
            meas.calculate_ratios()
            self.measurements[name] = meas
            self.ratios[name] = meas.get_ratios()
            self.channel_values[name] = meas.get_mean_values_and_stds()

    def get_ratio_amplitudes(self):
        x_ratio_amp = []
        y_ratio_amp = []
        for name, meas in self.measurements.items():
            ratios = meas.get_ratios()
            x_ratio_amp.append(ratios[0][0])
            y_ratio_amp.append(ratios[1][0])
        return x_ratio_amp, y_ratio_amp

    def get_ratios_normed(self):
        x_ratios = []
        y_ratios= []
        for name, meas in self.measurements.items():
            signal_a = meas.get_signal_mean("Channel A")
            signal_b = meas.get_signal_mean("Channel B")
            signal_c = meas.get_signal_mean("Channel C")
            signal_d = meas.get_signal_mean("Channel D")
            x_ratio = (signal_b - signal_d) / (signal_b + signal_d)
            y_ratio = (signal_c - signal_a) / (signal_c + signal_a)
            x_ratios.append(x_ratio)
            y_ratios.append(y_ratio)
        return x_ratios, y_ratios

    def get_ratios_std_normed(self):
        x_ratios = []
        y_ratios= []
        for name, meas in self.measurements.items():
            signal_a = meas.get_signal_mean("Channel A")
            signal_b = meas.get_signal_mean("Channel B")
            signal_c = meas.get_signal_mean("Channel C")
            signal_d = meas.get_signal_mean("Channel D")
            std_a = meas.get_signal_std("Channel A")
            std_b = meas.get_signal_std("Channel B")
            std_c = meas.get_signal_std("Channel C")
            std_d = meas.get_signal_std("Channel D")
            x_ratio_std = np.sqrt(((2 * signal_b / (signal_a + signal_b) ** 2) * std_a) ** 2 + ((-2 * signal_a / (signal_a + signal_b) ** 2) * std_b) ** 2)
            y_ratio_std = np.sqrt(((2 * signal_c / (signal_d + signal_c) ** 2) * std_d) ** 2 + ((-2 * signal_d / (signal_a + signal_c) ** 2) * std_c) ** 2)
            x_ratios.append(x_ratio_std)
            y_ratios.append(y_ratio_std)
        return x_ratios, y_ratios

    def get_ratios(self):
        x_ratios = []
        y_ratios= []
        for name, meas in self.measurements.items():
            signal_a = meas.get_signal_mean("Channel A")
            signal_b = meas.get_signal_mean("Channel B")
            signal_c = meas.get_signal_mean("Channel C")
            signal_d = meas.get_signal_mean("Channel D")
            x_ratio = signal_b  / signal_d
            y_ratio = signal_c / signal_a
            x_ratios.append(x_ratio)
            y_ratios.append(y_ratio)
        return x_ratios, y_ratios

    def get_ratios_std(self):
        x_ratios = []
        y_ratios= []
        for name, meas in self.measurements.items():
            signal_a = meas.get_signal_std_relative("Channel A")
            signal_b = meas.get_signal_std_relative("Channel B")
            signal_c = meas.get_signal_std_relative("Channel C")
            signal_d = meas.get_signal_std_relative("Channel D")
            x_ratio = np.sqrt(signal_d**2 + signal_b**2)
            y_ratio = np.sqrt(signal_c**2 + signal_a**2)
            x_ratios.append(x_ratio)
            y_ratios.append(y_ratio)
        return x_ratios, y_ratios

    def get_coordinate_arrays(self):
        x_values = []
        y_values = []
        for name, meas in self.measurements.items():
            splt = name.split("_")
            x = float(splt[1])
            y = float(splt[2])
            x_values.append(x)
            y_values.append(y)
        return x_values, y_values

    def get_channel_amplitudes(self, channel_name):
        """
        :return:
        """
        amplitudes = list()
        for name, meas in self.measurements.items():
            amps, stds = meas.get_mean_values_and_stds()
            amplitudes.append(amps[channel_name])
        return amplitudes

    def estimate_position_all_measurements(self, solution, table=True):
        for name, meas in self.measurements.items():
            print(f"estimating {name}")
            meas.estimate_x_y(solution, table)

    def get_estimated_accuracy(self):
        accuracy_x = {}
        accuracy_y = {}
        std_xs = {}
        std_ys = {}
        for name, meas in self.measurements.items():
            splt = name.split("_")
            x = int(splt[1])
            y = int(splt[2])
            mean_x, mean_y = meas.get_estimated_mean()
            std_x, std_y = meas.get_estimated_std()
            acc_x = x - mean_x
            acc_y = y - mean_y
            accuracy_x[name] = acc_x
            accuracy_y[name] = acc_y
            std_xs[name] = std_x
            std_ys[name] = std_y

            # x_ok = False
            # y_ok = False
            # if - estimated_acc <= acc_x <= estimated_acc:
            #     x_ok = True
            # if - estimated_acc <= acc_y <= estimated_acc:
            #     y_ok = True
            # print(f"estimating ({x}, {y}), x={x_ok}, {acc_x},  {std_x}    y={y_ok}, {acc_y},  {std_y} ")
        return accuracy_x, std_xs, accuracy_y, std_ys


    def save_all_estimations(self, folder):
        for name, meas in self.measurements.items():
            filename = f"{folder}{name}.png"
            meas.save_estimated_x_y(filename)

    def save(self, filename):
        """Saves the current instance to a file using pickle."""
        with open(filename, 'wb') as file:
            pickle.dump(self, file)

    @staticmethod
    def get_from_file(filename):
        """Loads a FitRatioManager instance from a file."""
        with open(filename, 'rb') as file:
            return pickle.load(file)

