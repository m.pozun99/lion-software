import struct

import pandas as pd
import matplotlib

from desktop_app.lion_gui.chart import MplCanvas
from measurement.util_functions import save_python_data_to_file, get_python_data_from_file
from fit_functions.visual_functions import plot_histogram, plot_multiple_histograms

matplotlib.use('Qt5Agg')
import numpy as np
from datetime import datetime

class MeasurementObject:
    CHANNELS = ['Channel A', 'Channel B', 'Channel C', 'Channel D']
    def __init__(self, name, folder):
        self.name = name
        self.folder = folder
        self.min_signal = dict()
        self.signals_mean_and_std = dict()
        self.means = dict()
        self.stds = dict()
        self.x_ratio = None
        self.y_ratio = None
        self.estimated_x = None
        self.estimated_y = None

    def get_data_from_csv(self, name, number_of_measurements=10000):
        min_original_a = np.zeros(number_of_measurements)
        min_original_b = np.zeros(number_of_measurements)
        min_original_c = np.zeros(number_of_measurements)
        min_original_d = np.zeros(number_of_measurements)
        t1 = datetime.now()
        subfolder = f"{name}/"
        print(f"START GETTING DATA: {name}")
        for i in range(self.number_of_measurements):
            if self.number_of_measurements // 10000 >= 1:
                filename = f"{name}_{i + 1:0>5}.csv"
            else:
                filename = f"{name}_{i + 1:0>4}.csv"
            csv_file = self.folder + subfolder + filename
            with open(csv_file, 'r') as f:
                name_line = f.readline().strip().split(',')
                units_line = f.readline().strip().split(',')
            factor_zip = zip(name_line, units_line)
            factors = dict()
            for name1, unit in factor_zip:
                if unit == "(V)":
                    factors[name1] = 1000
                elif unit == "(mV)":
                    factors[name1] = 1
                else:
                    factors[name1] = 0

            data = pd.read_csv(csv_file, skiprows=1)
            data.columns = ['Time', 'Channel A', "Channel B", "Channel C", "Channel D", "Channel G"]
            signal_array_a = data['Channel A'].to_numpy() * factors['Channel A']
            signal_array_b = data['Channel B'].to_numpy() * factors['Channel B']
            signal_array_c = data['Channel C'].to_numpy() * factors['Channel C']
            signal_array_d = data['Channel D'].to_numpy() * factors['Channel D']
            min_original_a[i] = np.min(signal_array_a)
            min_original_b[i] = np.min(signal_array_b)
            min_original_c[i] = np.min(signal_array_c)
            min_original_d[i] = np.min(signal_array_d)
        self.min_signal['Channel A'] = min_original_a
        self.min_signal['Channel B'] = min_original_b
        self.min_signal['Channel C'] = min_original_c
        self.min_signal['Channel D'] = min_original_d
        t2 = datetime.now()
        print(f"Time passed {t2 - t1}")

    def save_data_to_files(self, folder=None):
        prefix = self.name
        save_python_data_to_file(self.min_signal['Channel A'], f"{prefix}_channel_A.pk", folder)
        save_python_data_to_file(self.min_signal['Channel B'], f"{prefix}_channel_B.pk", folder)
        save_python_data_to_file(self.min_signal['Channel C'], f"{prefix}_channel_C.pk", folder)
        save_python_data_to_file(self.min_signal['Channel D'], f"{prefix}_channel_D.pk", folder)

    def get_data_from_files(self, folder=None):
        prefix = self.name
        self.min_signal['Channel A'] = get_python_data_from_file(f"{prefix}_channel_A.pk", folder)
        self.min_signal['Channel B'] = get_python_data_from_file(f"{prefix}_channel_B.pk", folder)
        self.min_signal['Channel C'] = get_python_data_from_file(f"{prefix}_channel_C.pk", folder)
        self.min_signal['Channel D'] = get_python_data_from_file(f"{prefix}_channel_D.pk", folder)

    def get_data_from_binary(self, folder=None):
        if folder is None:
            folder = self.folder
        file_path = f"{folder}{self.name}"
        data_dict = {'a': [], 'b': [], 'c': [], 'd': []}
        with open(file_path, "rb") as file:
            while True:
                header = file.read(2)
                if not header:
                    break  # End of file reached

                try:
                    # Decode the letter and get the count directly
                    letter, count = struct.unpack('cB', header)
                    letter = letter.decode('utf-8')
                except (struct.error, UnicodeDecodeError) as e:
                    print(f"Error unpacking header: {e}")
                    continue

                # Check if the header is valid
                if letter == 'd' and count > 0:
                    channels = ['a', 'b', 'c', 'd']
                    for chn in channels:
                        integers = []
                        for _ in range(count):
                            data_bytes = file.read(2)  # Read 2 bytes per integer
                            if not data_bytes:
                                break  # End of file or data corruption
                            try:
                                integer = struct.unpack('<h', data_bytes)[0]
                                integers.append(integer)
                            except struct.error as e:
                                print(f"Error unpacking data: {e}")
                                break  # Stop processing this packet

                        # Append the integers to the list for 'd' if any were read
                        if integers:
                            data_dict[chn].extend(integers)
                t = file.read(6)
        self.min_signal['Channel A'] = np.array(data_dict['a'])
        self.min_signal['Channel B'] = np.array(data_dict['b'])
        self.min_signal['Channel C'] = np.array(data_dict['c'])
        self.min_signal['Channel D'] = np.array(data_dict['d'])
        print(f"Data gathered from {file_path}")


    def plot_channel_histogram(self, channel, show=False, filename=None):
        values = self.min_signal[channel]
        plot_histogram(f"{self.name}_{channel}", values, show=show, filename=filename)

    def plot_all_channel_histograms(self, show=False, filename=None):
        names = list(self.min_signal.keys())
        values_list = list(self.min_signal.values())
        plot_multiple_histograms(names, values_list, show=show, filename=filename)

    def calculate_ratios(self):
        self.x_ratio = self.min_signal["Channel B"] / self.min_signal["Channel D"]
        self.y_ratio = self.min_signal["Channel A"] / self.min_signal["Channel C"]

    def get_ratios(self):
        x_result = np.mean(self.x_ratio), np.std(self.x_ratio)
        y_result = np.mean(self.y_ratio), np.std(self.y_ratio)
        return x_result, y_result

    def get_signal_mean(self, channel):
        data = self.min_signal[channel]
        data_with_nan = np.where(np.isinf(data), np.nan, data)
        mean_value = np.nanmean(data_with_nan)
        return mean_value
    def get_signal_std_relative(self, channel):
        data = self.min_signal[channel]
        data_with_nan = np.where(np.isinf(data), np.nan, data)
        mean_value = np.nanmean(data_with_nan)
        std_value = np.nanstd(data_with_nan)
        return std_value / mean_value

    def get_signal_std(self, channel):
        data = self.min_signal[channel]
        data_with_nan = np.where(np.isinf(data), np.nan, data)
        std_value = np.nanstd(data_with_nan)
        return std_value

    def apply_offsets(self, offsets):
        for channel in self.CHANNELS:
            self.min_signal[channel] -= offsets[channel]

    def get_mean_values_and_stds(self):
        for channel in self.CHANNELS:
            signal = self.min_signal[channel]
            self.means[channel] = np.mean(signal)
            self.stds[channel] = np.std(signal)
        return self.means, self.stds


    def plot_ratio_histograms(self, show=False, filename=None):
        names = ["Channel A / Channel C", "Channel D / Channel B"]
        values = [self.x_ratio, self.y_ratio]
        plot_multiple_histograms(names, values, show=show, filename=filename, bins_number=32)

    def get_single_ratios_normed(self):
        signal_a = np.array(self.min_signal["Channel A"])
        signal_b = np.array(self.min_signal["Channel B"])
        signal_c = np.array(self.min_signal["Channel C"])
        signal_d = np.array(self.min_signal["Channel D"])
        x_ratio = (signal_b - signal_d) / (signal_b + signal_d)
        y_ratio = (signal_c - signal_a) / (signal_c + signal_a)
        return x_ratio, y_ratio

# estimation function to
    def estimate_x_y(self, solution, table=True):
        x_ratio, y_ratio = self.get_single_ratios_normed()
        self.estimated_x, self.estimated_y = solution.solve_ratios(x_ratio, y_ratio, table=True)

    def save_estimated_x_y(self, filename):
        # Ensure that estimated_x and estimated_y have been calculated
        if self.estimated_x is None or self.estimated_y is None:
            raise ValueError("Estimated values for x and y have not been calculated.")

        # Initialize MplCanvas
        splt = self.name.split("_")
        x = splt[1]
        y = splt[2]
        print(x, y)
        canvas = MplCanvas(title=None, width=5, height=5, dpi=100)
        # Update the plot with estimated x and y
        canvas.update_plot(self.estimated_x, self.estimated_y)
        canvas.draw_pos_specifiers(int(x), int(y))
        if filename is not None:
            canvas.figure.savefig(filename, format='png', dpi=300, bbox_inches='tight')

    def get_estimated_std(self):
        std_x = np.std(self.estimated_x)
        std_y = np.std(self.estimated_y)
        return std_x, std_y

    def get_estimated_mean(self):
        mean_x = np.mean(self.estimated_x)
        mean_y = np.mean(self.estimated_y)
        return mean_x, mean_y





