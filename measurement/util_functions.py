import matplotlib.pyplot as plt
import numpy as np
from lmfit import Model

# Load data from CSV file
import pickle


def save_python_data_to_file(data, name, folder="temp_data/"):
    print(f"SAVING DATA TO: {name}")
    with open(folder + name, 'wb') as fi:
        pickle.dump(data, fi)


def get_python_data_from_file(name, folder="temp_data/"):
    print(f"GETTING DATA FROM: {name}")
    with open(folder + name, 'rb') as fi:
        data = pickle.load(fi)
        return data


def gaussian(x, amp, cen, wid):
    """1-d gaussian: gaussian(x, amp, cen, wid)"""
    return (amp / (np.sqrt(2 * np.pi) * wid)) * np.exp(-(x - cen) ** 2 / (2 * wid ** 2))


def calculate_deviation_of_signal(values):
    center = np.mean(values)
    std = np.std(values)
    relative_std = std / center
    return center, std, relative_std


def calculate_deviation_of_ratio(values_a, values_b):
    values_ratio = values_a / values_b
    # values A, B
    center_a, std_a, relative_std_a = calculate_deviation_of_signal(values_a)
    center_b, std_b, relative_std_b = calculate_deviation_of_signal(values_b)
    # values Ratio
    center_ratio, std_ratio, relative_std_ratio = calculate_deviation_of_signal(values_ratio)

    # calculate ratio
    calc_center_ratio = center_a / center_b
    calc_relative_std_ratio = np.sqrt(relative_std_a ** 2 + relative_std_b ** 2)
    print(f"VALUES A: center: {center_a:.4f} std: {std_a:.4f} relative_std: {relative_std_a:.4f}")
    print(f"VALUES B: center: {center_b:.4f} std: {std_b:.4f} relative_std: {relative_std_b:.4f}")
    print(f"VALUES RATIO: center: {center_ratio:.4f} std: {std_ratio:.4f} relative_std: {relative_std_ratio:.4f}")
    print(f"CALCULATE RATIO VALUES: center: {calc_center_ratio:.4f}  relative_std: {calc_relative_std_ratio:.4f}")


def plot_histogram(name, values, show=False, filename=None):
    plt.cla()

    count, bins, ignored = plt.hist(values, bins=1024, density=False, alpha=0.6, color='g')

    # Calculate bin centers
    bin_centers = (bins[:-1] + bins[1:]) / 2

    # Filter out bins with zero counts
    non_zero_bins = count > 0
    bin_centers_non_zero = bin_centers[non_zero_bins]
    count_non_zero = count[non_zero_bins]

    # Fit a Gaussian function to the non-zero bins
    gmodel = Model(gaussian)
    amplitude = np.max(count_non_zero)
    center = np.mean(values)
    width = np.std(values)
    params = gmodel.make_params(amp=amplitude, cen=center, wid=width)
    result = gmodel.fit(count_non_zero, x=bin_centers_non_zero, params=params)

    # Plot the fitted Gaussian curve
    x_interval_for_fit = np.linspace(bin_centers[0], bin_centers[-1], 10000)
    # plt.plot(bin_centers_non_zero, result.init_fit, '--', label='initial fit')
    plt.plot(bin_centers_non_zero, result.best_fit, '-', label='best fit')
    print(name)
    center_value = result.params['cen'].value
    width_value = result.params['wid'].value
    relative_std = width_value / center_value
    print(f"Center {center_value:.4f} sigma {width_value:.4f}   Relative: {relative_std:.4f}")
    print(f"Width / Center {width_value / center_value:.4f}")
    AMP_N_1 = 1.5
    N = center_value / AMP_N_1
    print(f"N =  {N} , AMP_N_1 = {AMP_N_1};  expected sigma sqrt(N)*AMP_N_1 = {np.sqrt(N) * AMP_N_1} ")
    print(f"calculated sigma: {width}")
    # AMP_N_1 = 0.58
    # print(f"sqrt(center / AMP_N_1) =  {np.sqrt(center_value / AMP_N_1)} , AMP_N_1 = {AMP_N_1}")
    # Add title and labels with fit results
    info = f"Fit results: mean = {result.params['cen'].value:.4f}, std = {result.params['wid'].value:.4f}"
    title = f'Histogram of Minimum Values {name}\n{info}'
    plt.title(title)
    plt.xlabel('Minimum Value of Signal')
    plt.ylabel('Frequency')
    plt.grid(True)
    plt.legend()

    # Save the plot to a file if a filename is provided
    if filename is not None:
        plt.savefig(filename, format='png', bbox_inches='tight')

    # Show the plot if requested
    if show:
        plt.show()
