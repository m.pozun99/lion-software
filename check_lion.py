from measurement.measurement import MeasurementObject

folder = "measurement_data/data_lion_new/"
meas = MeasurementObject("pos_30_30_data.dat", folder)
meas.get_data_from_binary()
meas.plot_all_channel_histograms(show=True)