from matplotlib import pyplot as plt

from fit_functions.fit_functions import fit_dist_function, dist
from measurement.measurement import MeasurementObject
import numpy as np
import os


# def get_indexes_of_array(x_values):
#     x_values_array = np.array(x_values)
#     indexes = np.where(x_values_array == 0)[0]
#     return indexes

def get_channel_values_at_indexes(indexes, channel_amp):
    channel_values_at_indexes = {}

    for channel in channel_amp:
        channel_values_at_indexes[channel] = np.array(channel_amp[channel])[indexes].tolist()

    return channel_values_at_indexes
def get_subdirectories(base_path):
    """
    Returns the names of all subdirectories from the specified folder as a list.

    :param base_path: Path of the base directory
    :return: List of subdirectory paths
    """
    subdirectories = []
    for root, dirs, _ in os.walk(base_path):
        for name in dirs:
            subdirectories.append(name)

    return subdirectories


FOLDER = "/home/matej/Waveforms/meas_calibration/"
NUMBER_OF_MEASUREMENTS = 1000

PLOT_FOLDER = "histograms_char/"
coordinates_x = [(-60, 0), (-30, 0), (0, 0), (30, 0), (60, 0)]
coordinates_y = [(0, -60), (0, -30), (0, 0), (0, 30), (0, 60)]

offsets = {"Channel A": 0.9, "Channel B": 4, "Channel C": 4.5, "Channel D": 2.5}

ratios = dict()
channel_values = dict()
names = get_subdirectories(FOLDER)
for name in names:
    data_folder = "data_calibration/"
    meas = MeasurementObject(name, FOLDER, NUMBER_OF_MEASUREMENTS)
    # meas.get_data_from_csv(name)
    # meas.save_data_to_files(data_folder)
    meas.get_data_from_files(data_folder)
    meas.apply_offsets(offsets)
    # meas.plot_all_channel_histograms(filename=PLOT_FOLDER + f"histograms_{name}.png")
    meas.calculate_ratios()
    # meas.plot_ratio_histograms(filename=PLOT_FOLDER + f"ratio_{name}.png")
    ratios[name] = meas.get_ratios()
    channel_values[name] = meas.get_mean_values_and_stds()

x_values = []
y_values = []
channel_amp = {"Channel A": [], "Channel B": [], "Channel C": [], "Channel D": []}
channel_std = {"Channel A": [], "Channel B": [], "Channel C": [], "Channel D": []}
for name, channel_data in channel_values.items():
    splt = name.split("_")
    if abs(float(splt[1])) == 60 and abs(float(splt[2])) == 0:
        continue
    if abs(float(splt[1])) == 0 and abs(float(splt[2])) == 60:
        continue
    if float(splt[1]) == 0 and float(splt[2]) == 30:
        continue
    x_values.append(float(splt[1]))
    y_values.append(float(splt[2]))

    for chn_name, amp in channel_data[0].items():
        channel_amp[chn_name].append(abs(amp))
    for chn_name, std in channel_data[1].items():
        channel_std[chn_name].append(std)

# MeasurementObject.plot_on_grid(x_values, y_values, channel_amp["Channel A"], "channel a", show=True)

x_values = np.array(x_values)
y_values = np.array(y_values)
# # get values where x = 0
# indexes_x_0 = np.where(x_values == 0)[0]
# channel_where_x_0 = get_channel_values_at_indexes(indexes_x_0, channel_amp)
# y_0 = y_values[indexes_x_0]
# init_params_0 = {"Channel A": {'a': 1000, 'b': 65}, "Channel C": {'a': 1000, 'b': -65}}
# # get values where y = 0
# indexes_y_0 = np.where(y_values == 0)[0]
# channel_where_y_0 = get_channel_values_at_indexes(indexes_y_0, channel_amp)
# x_0 = x_values[indexes_y_0]
# init_params_y0 = {"Channel B": {'a': 1000, 'b': -65}, "Channel D": {'a': 1000, 'b': 65}}
# init_params_0.update(init_params_y0)
# for chn in list(channel_where_x_0.keys()):
#     if chn == "Channel A" or chn == "Channel C":
#         data = np.array(channel_where_x_0[chn])
#         dist = y_0
#     else:
#         data = np.array(channel_where_y_0[chn])
#         dist = x_0
#     fitted_params = fit_function(dist, data, init_params_0[chn], filename=PLOT_FOLDER + f"plot_0_funcion_{chn}.png")
#     a_fit = fitted_params['a'].value
#     b_fit = fitted_params['b'].value
#     print(f"Fitted parameter 'a': {a_fit}")
#     print(f"Fitted parameter 'b': {b_fit}")


for key, values in channel_amp.items():
    if key == "Channel A":
        forward_coord = y_values
        side_coord = x_values
        init_params = {'a': 1000, 'b': 0, 'c': 65}
    elif key == "Channel B":
        forward_coord = x_values
        side_coord = y_values
        init_params = {'a': 1000, 'b': -65, 'c': 0}
    elif key == "Channel C":
        forward_coord = y_values
        side_coord = x_values
        init_params = {'a': 1000, 'b': 0, 'c': -65}
    else:
        forward_coord = x_values
        side_coord = y_values
        init_params = {'a': 1000, 'b': 65, 'c': 0}
    values = np.array(values)
    model, result = fit_dist_function(x_values, y_values, values, init_params, show=True)
    params = result.params
    a_fit = params['a'].value
    x0 = params['b'].value
    y0 = params['c'].value
    print(result.fit_report())
    print(f"Fitted parameters for {key} 'amplitude': {a_fit}, x0: {x0},  y0: {y0}")

    plt.cla()
    xy = (x_values, y_values)
    xy_dist = dist(xy, x0, y0)
    indexes_side_0 = np.where(side_coord == 0)[0]
    xy_dist_side = xy_dist[indexes_side_0]
    straight_values = values[indexes_side_0]



    plt.scatter(xy_dist, values, color="blue", label="Measured values")
    plt.scatter(xy_dist_side, straight_values, color="orange", label="Side value=0 values")
    fitted_values = model.eval(params=result.params, xy=xy)
    # plt.scatter(xy_dist, fitted_values, color="red", label="Fitted values")
    x_interp = np.linspace(1, 150, 500) + x0
    y_interp = np.ones_like(x_interp) * y0
    xy_interp = (x_interp, y_interp)
    distance = dist(xy_interp, x0, y0)
    fitted_interpolated = model.eval(params=result.params, xy=xy_interp)
    plt.plot(distance, fitted_interpolated, color="green", label="Fit based on distance")
    plt.legend()
    plt.title(key)
    plt.show()
    # Plotting or Saving results if required
    # if show or filename:
    #     plt.scatter(x_values, y_values, c=signal_values, cmap='viridis')  # original data as a color map
    #     plt.colorbar(label='Signal Values')
    #     # For plotting the result, you'll need to generate a grid of x, y points and evaluate the model
    #     if filename or show:
    #
    #         fitted_values = model.eval(params=result.params, xy=grid_xy)
    #         plt.contour(grid_x, grid_y, fitted_values, levels=20, cmap='RdGy')




# MeasurementObject.plot_ratios_on_grid(ratios, filename=PLOT_FOLDER + f"ratio_plot.png", show=True)
#MeasurementObject.plot_channel_on_grid(channel_values, "Channel A", filename=PLOT_FOLDER + f"Channel A.png", show=True)