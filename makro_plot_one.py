from measurement.measurement import MeasurementObject

folder = "desktop_app/data_lion_new/"
meas = MeasurementObject("pos_0_-15_data.dat", folder)
meas.get_data_from_binary()
meas.plot_all_channel_histograms(show=True, filename="plots/histograms_channels.pgf")