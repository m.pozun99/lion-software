import numpy as np
import matplotlib.pyplot as plt


# Define the functions
class SiPM:
    LOSS_DIST = 120 # in mm
    def __init__(self, x, y, orientation, length):
        self.x = x
        self.y = y
        self.orientation = orientation
        self.length = length

    def calculate_response_to_points(self, x, y):
        if self.orientation == "x_parallel":
            l_dist = np.abs(self.y - y)
            w_dist = np.abs(self.x - x)
            pass
        elif self.orientation == "y_parallel":
            l_dist = np.abs(self.x - x)
            w_dist = np.abs(self.y - y)
        else:
            return None

        angle_precise = self.calculate_angle_precise(l_dist, w_dist, self.length)
        loss_factor = self.calculate_loss_factor(self.calculate_dist(l_dist, w_dist))
        response = angle_precise * loss_factor
        return response

    @staticmethod
    def calculate_angle_precise(l_dist, w_dist, d=4):
        fi1 = np.arctan((w_dist - d / 2) / l_dist)
        fi2 = np.arctan(l_dist / (w_dist + d / 2))
        angle = np.pi / 2 - fi1 - fi2
        return angle

    @staticmethod
    def calculate_dist(l_dist, w_dist):
        dist = np.sqrt(l_dist ** 2 + w_dist ** 2)
        return dist

    def calculate_loss_factor(self, dist):
        return np.exp(-dist / self.LOSS_DIST)



class DetectorGrid:
    PLOT_FOLDER = "theoretical_plots/"
    def __init__(self, x_range, y_range, n_points):
        self.x_range = x_range
        self.y_range = y_range
        self.n_points = n_points
        x = np.linspace(self.x_range[0], self.x_range[1], self.n_points)
        y = np.linspace(self.y_range[0], self.y_range[1], self.n_points)
        self.X, self.Y = np.meshgrid(x, y)
        self.x_parallel_sipms = []
        self.y_parallel_sipms = []

    def add_sipm(self, sipm):
        if sipm.orientation == "y_parallel":
            self.y_parallel_sipms.append(sipm)
        elif sipm.orientation == "x_parallel":
            self.x_parallel_sipms.append(sipm)

    def plot_sipm_response(self, sipm):
        Z = sipm.calculate_response_to_points(self.X, self.Y)
        self.plot_contour(Z, filename=self.PLOT_FOLDER + f"sipm_{sipm.orientation}_{sipm.x}_{sipm.y}_ratio.png")


    def calculate_ratio_point(self, x, y):
        if len(self.y_parallel_sipms) == 2:
            r1 = self.y_parallel_sipms[0].calculate_response_to_points(x, y)
            r2 = self.y_parallel_sipms[1].calculate_response_to_points(x, y)
            response_y_parallel = r1 / r2
        else:
            response_y_parallel = None
        if len(self.x_parallel_sipms) == 2:
            r1 = self.x_parallel_sipms[0].calculate_response_to_points(x, y)
            r2 = self.x_parallel_sipms[1].calculate_response_to_points(x, y)
            response_x_parallel = r1 / r2
        else:
            response_x_parallel = None
        return response_x_parallel, response_y_parallel





    def plot_ratio(self, orientation):
        if orientation == "y_parallel":
            sipms = self.y_parallel_sipms
        elif orientation == "x_parallel":
            sipms = self.x_parallel_sipms
        else:
            return
        z0 = sipms[0].calculate_response_to_points(self.X, self.Y)
        z1 = sipms[1].calculate_response_to_points(self.X, self.Y)
        ratio = z0 / z1
        self.plot_contour(ratio, filename=self.PLOT_FOLDER + f"{orientation}_ratio.png")

    def plot_contour(self, Z, filename=None):
        fig, axs = plt.subplots(1, 1, figsize=(18, 6))
        cmap = 'plasma'
        cp = axs.contourf(self.X, self.Y, Z, 100, cmap=cmap)
        fig.colorbar(cp, ax=axs)
        axs.set_title('Angle Distance')
        axs.set_xlabel('X axis')
        axs.set_ylabel('Y axis')
        plt.tight_layout()
        if filename is not None:
            plt.savefig(filename, format='png', bbox_inches='tight')






def calculate_angle_dist(x, y, d=3):
    dist = np.sqrt(x ** 2 + y ** 2)
    angle = 2 * np.arctan(d / 2 / dist)
    return angle


def calculate_angle_precise(x, y, d=3):
    fi1 = np.arctan((np.abs(y - d) / 2) / x)
    fi2 = np.arctan((y + d / 2) / x)
    angle = np.pi / 2 - fi1 - fi2
    return angle


# Create a meshgrid for x and y values

# Calculate angles for each function
# Z_dist = calculate_angle_dist(X, Y)
# Z_precise = calculate_angle_precise(X, Y)
#
# # Define the number of contour levels for contrast control
# n_levels = 100  # Adjust this number to increase or decrease contrast
#
# # Choose a colormap
# cmap = 'plasma'  # This colormap provides good contrast, but you can choose another
#
# # Plotting in 2D with adjustable contrast
# fig, axs = plt.subplots(1, 3, figsize=(18, 6))
#
# # Plot for calculate_angle_dist
# cp = axs[1].contourf(X, Y, Z_dist, n_levels, cmap=cmap)
# fig.colorbar(cp, ax=axs[1])
# axs[1].set_title('Angle Distance')
# axs[1].set_xlabel('X axis')
# axs[1].set_ylabel('Y axis')
#
# # Plot for calculate_angle_precise
# cp = axs[2].contourf(X, Y, Z_precise, n_levels, cmap=cmap)
# fig.colorbar(cp, ax=axs[2])
# axs[2].set_title('Angle Precise')
# axs[2].set_xlabel('X axis')
# axs[2].set_ylabel('Y axis')
#
# plt.tight_layout()
# plt.show()

def init_grid():
    sipm_left = SiPM(-100, 0, "y_parallel", 4)
    sipm_right = SiPM(100, 0, "y_parallel", 4)
    sipm_top = SiPM(0, 100, "x_parallel", 4)
    sipm_bottom = SiPM(0, -100, "x_parallel", 4)
    grid = DetectorGrid((-70, 70), (-70, 70), 400)
    grid.add_sipm(sipm_right)
    grid.add_sipm(sipm_left)
    grid.add_sipm(sipm_top)
    grid.add_sipm(sipm_bottom)
    return grid



if __name__ == "__main__":
    grid = init_grid()
    grid.plot_ratio("y_parallel")
    grid.plot_sipm_response(grid.y_parallel_sipms[0])
    for x in range(-60, 61, 30):
        print(grid.calculate_ratio_point(x, 0))