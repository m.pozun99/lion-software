import numpy as np

rise_time = 1e-9
Fp_calculated = 0.3 / rise_time

# Fp_desired = 100e6
Ci = 770e-12
# Cfb = 10e-12
Rfb = 2200
GBW = 50e6

# Fp = 1 / (2 * np.pi * Rfb * Cfb)
# GBW = (2 * Ci + Cfb) / (2*np.pi * Rfb * Ci**2)

Cf = 1 / (4*np.pi * Rfb * GBW) * (1 + np.sqrt(1 + 8 * np.pi * Rfb * Ci * GBW))
print(f"Cf = {Cf * 1e12}")

# print(f"Signal frequency calculated {Fp_calculated / 1e6} MHz")
# print(f"Cutoff frequency {Fp / 1e6} MHz")
# print(f"GBW {GBW / 1e6} MHz")