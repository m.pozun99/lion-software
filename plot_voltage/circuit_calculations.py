import numpy as np

# rise_time = 1e-9
# Fp_calculated = 0.3 / rise_time
#
# Fp_desired = 100e6
# Ci = 770e-12
# Cfb = 10e-12
# Rfb = 22000
#
#
# Fp = 1 / (2 * np.pi * Rfb * Cfb)
# GBW = (2 * Ci + Cfb) / (2*np.pi * Rfb * Ci**2)
# print(f"Signal frequency calculated {Fp_calculated / 1e6} MHz")
# print(f"Cutoff frequency {Fp / 1e6} MHz")
# print(f"GBW {GBW / 1e6} MHz")

GBW = 650e6
Rf = 2200
Cs = 770e-12
f45 = np.sqrt(GBW/(2*np.pi*Rf * Cs ))
Cf = np.sqrt(Cs / (2*np.pi * Rf * GBW))
print(Cf)