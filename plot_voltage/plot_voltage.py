import matplotlib.pyplot as plt
import pandas as pd
import matplotlib
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})
# Path to your data file
file_path = 'graph.txt'  # Replace 'path/to/your/' with the actual path to 'graph.txt'

# Reading the data from the file
data_from_file = pd.read_csv(file_path, delim_whitespace=True)
time = data_from_file["time"] * 1e6
# Plotting
fig, ax = plt.subplots()
#plt.figure(figsize=(10, 6))
ax.plot(time, data_from_file["V(discharge)"], label="$V_{discharge}$")
ax.plot(time, data_from_file["V(peak)"], label="$V_{peak}$")
ax.plot(time, data_from_file["V(peak_in)"], label="$V_{peak\_in}$")
ax.set_xlim((0, 6))
ax.set_ylim((-0.5, 3.5))
ax.set_xlabel('Čas $[\mu s]$')
ax.set_ylabel("Napetost $[V]$")

# plt.title("Voltage vs. Time")
ax.legend(loc='upper left')
plt.savefig('sim_peak.pgf')
#plt.show()
