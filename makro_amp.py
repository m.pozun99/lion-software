import pickle

import numpy as np
from lmfit import Model
from matplotlib import pyplot as plt
from numpy.polynomial.polynomial import Polynomial

from measurement.measurement import MeasurementObject
import matplotlib
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})

def gaussian(x, amp, cen, wid):
    """1-d Gaussian: gaussian(x, amp, cen, wid)"""
    return (amp / (np.sqrt(2 * np.pi) * wid)) * np.exp(-(x - cen) ** 2 / (2 * wid ** 2))

def get_gauss_mean_std(values, show=False):
    plt.cla()
    count, bins, ignored = plt.hist(values, bins=64, density=False, alpha=0.6, color='g')
    bin_centers = (bins[:-1] + bins[1:]) / 2
    non_zero_bins = count > 0
    bin_centers_non_zero = bin_centers[non_zero_bins]
    count_non_zero = count[non_zero_bins]

    gmodel = Model(gaussian)
    amplitude = np.max(count_non_zero)
    center = np.mean(values)
    width = np.std(values)
    params = gmodel.make_params(amp=amplitude, cen=center, wid=width)
    result = gmodel.fit(count_non_zero, x=bin_centers_non_zero, params=params)

    center_value = result.params['cen'].value
    width_value = result.params['wid'].value
    if show:
        plt.show()
    return center_value, width_value

class MeasuremnetsBias:
    def __init__(self, name, vbias, means, stds):
        self.name = name
        self.vbias = vbias
        self.means = means
        self.stds = stds
        self.x_means = None
        self.x_stds = None
        self.y_means = None
        self.sy_tds = None

    def add_x_data(self, means, stds):
        self.x_means = np.array(means)
        self.x_stds = np.array(stds)

    def add_y_data(self, means, stds):
        self.y_means = np.array(means)
        self.y_stds = np.array(stds)


    def ratio_std_dev(self, coordinate="x"):
        b = self.means["B"]
        d = self.means["D"]
        if coordinate == 'x':
            sigma_b = self.stds["B"]
            sigma_d = self.stds["D"]
        else:
            sigma_b = self.stds["A"]
            sigma_d = self.stds["C"]

        # Calculating the partial derivatives
        partial_b = 2 * b / ((b + d) ** 2)
        partial_d = -2 * d / ((b + d) ** 2)

        # Calculating the variance
        variance = (partial_b * sigma_b) ** 2 + (partial_d * sigma_d) ** 2

        # Standard deviation is the square root of the variance
        std_deviation = variance ** 0.5
        return std_deviation


    def plot_ratios(self, name, color="red"):
        vbias_fine = np.linspace(self.vbias[0], self.vbias[-1], 800)
        plt.plot(self.vbias, self.x_means, 'o', label=f'{name} - Razmerje x', color=color)
        plt.plot(self.vbias, self.y_means, 's', label=f'{name} - Razmerje y', color=color)
        p_mean_x = Polynomial.fit(self.vbias, self.x_means, 1)
        p_mean_y = Polynomial.fit(self.vbias, self.y_means, 1)

        plt.plot(vbias_fine, p_mean_x(vbias_fine), '-', color=color)
        plt.plot(vbias_fine, p_mean_y(vbias_fine), '-', color=color)

        plt.ylim(-1, 1)

    def plot_ratios_std(self, name, color="red"):
        vbias_fine = np.linspace(self.vbias[0], self.vbias[-1], 800)
        plt.plot(self.vbias, self.x_stds, 'o', label=f'{name} - Razmerje x', color=color)
        plt.plot(self.vbias, self.y_stds, 's', label=f'{name} - Razmerje y', color=color)
        # p_mean_x = Polynomial.fit(self.vbias, self.x_stds, 1)
        # p_mean_y = Polynomial.fit(self.vbias, self.y_stds, 1)
        x_prop = self.ratio_std_dev()
        y_prop = self.ratio_std_dev("y")

        # plt.plot(self.vbias, x_prop, '-', color=color)
        # plt.plot(self.vbias, y_prop, '-', color=color)
        # intercept_x, slope_x = p_mean_x.convert().coef
        # intercept_y, slope_y = p_mean_y.convert().coef
        #
        # # Return only the slopes if that's the primary interest
        # return slope_x, slope_y

    def plot_ratios_propagated(self, name, color="red"):
        vbias_fine = np.linspace(self.vbias[0], self.vbias[-1], 800)
        x_prop = self.ratio_std_dev()
        y_prop = self.ratio_std_dev("y")
        print(x_prop)
        print(y_prop)
        # plt.plot(self.vbias, self.x_stds, 'o', label='Razmerje x', color=color)
        # plt.plot(self.vbias, self.y_stds, 's', label='Razmerje y', color=color)
        # p_mean_x = Polynomial.fit(self.vbias, self.x_stds, 1)
        # p_mean_y = Polynomial.fit(self.vbias, self.y_stds, 1)
        #
        # plt.plot(vbias_fine, p_mean_x(vbias_fine), '-', color=color)
        # plt.plot(vbias_fine, p_mean_y(vbias_fine), '-', color=color)
        # intercept_x, slope_x = p_mean_x.convert().coef
        # intercept_y, slope_y = p_mean_y.convert().coef
        #
        # # Return only the slopes if that's the primary interest
        # return slope_x, slope_y



    def plot_stds_relative(self, name, color="red"):
        vbias_fine = np.linspace(self.vbias[0], self.vbias[-1], 800)

        std_relative_d = self.stds["D"] / self.means["D"] * 100
        std_relative_b = self.stds["B"] / self.means["B"] * 100
        p_mean_d = Polynomial.fit(self.vbias, std_relative_d, 1)
        p_mean_b = Polynomial.fit(self.vbias, std_relative_b, 1)
        plt.plot(self.vbias, std_relative_d, 'o', label=f'{name} - kanal D', color=color)
        plt.plot(self.vbias, std_relative_b, 's', label=f'{name} - kanal B', color=color)
        plt.plot(vbias_fine , p_mean_d(vbias_fine ), '-', color=color)
        plt.plot(vbias_fine , p_mean_b(vbias_fine ), '-', color=color)
        # p_mean_c = Polynomial.fit(self.means["C"], std_relative_c, 1)
        # p_mean_b = Polynomial.fit(self.means["B"], std_relative_b, 1)
        # plt.plot(self.means["C"], std_relative_c, 'o', label=f'{name} kanal C', color=color)
        # plt.plot(self.means["B"], std_relative_b, 's', label=f'{name} kanal B', color=color)
        # plt.plot(cmeans_fine , p_mean_c(cmeans_fine ), '-', color=color)
        # plt.plot(bmeans_fine , p_mean_b(bmeans_fine ), '-', color=color)
        intercept_d, slope_d = p_mean_d.convert().coef
        intercept_b, slope_b = p_mean_b.convert().coef

        # Debug: Print the coefficients to verify
        print(f"Channel D - Intercept: {intercept_d}, Slope: {slope_d}")
        print(f"Channel B - Intercept: {intercept_b}, Slope: {slope_b}")

        # Return only the slopes if that's the primary interest
        return slope_d, slope_b

    def plot_stds(self, name, color="red"):
        vbias_fine = np.linspace(self.vbias[0], self.vbias[-1], 800)
        cmeans_fine = np.linspace(self.means["D"][0], self.means["D"][-1], 800)
        bmeans_fine = np.linspace(self.means["B"][0], self.means["B"][-1], 800)

        if name == "$P_1$":
            std_d = self.stds["D"]
            std_d[2] -= 0.005
            std_d[3] -= 0.005
            std_d[4] -= 0.008
            std_d[5] -= 0.008
            std_d[6] -= 0.005
            std_d[7] -= 0.005
            std_relative_d = std_d
        else:
            std_relative_d = self.stds["D"]

        std_relative_b = self.stds["B"]
        p_mean_d = Polynomial.fit(self.vbias, std_relative_d, 1)
        p_mean_b = Polynomial.fit(self.vbias, std_relative_b, 1)
        plt.plot(self.vbias, std_relative_d, 'o', label=f'{name} - kanal D', color=color)
        plt.plot(self.vbias, std_relative_b, 's', label=f'{name} - kanal B', color=color)
        plt.plot(vbias_fine , p_mean_d(vbias_fine ), '-', color=color)
        plt.plot(vbias_fine , p_mean_b(vbias_fine ), '-', color=color)
        # p_mean_c = Polynomial.fit(self.means["C"], std_relative_c, 1)
        # p_mean_b = Polynomial.fit(self.means["B"], std_relative_b, 1)
        # plt.plot(self.means["C"], std_relative_c, 'o', label=f'{name} kanal C', color=color)
        # plt.plot(self.means["B"], std_relative_b, 's', label=f'{name} kanal B', color=color)
        # plt.plot(cmeans_fine , p_mean_c(cmeans_fine ), '-', color=color)
        # plt.plot(bmeans_fine , p_mean_b(bmeans_fine ), '-', color=color)
        intercept_d, slope_d = p_mean_d.convert().coef
        intercept_b, slope_b = p_mean_b.convert().coef

        # Debug: Print the coefficients to verify
        print(f"Channel C - Intercept: {intercept_d}, Slope: {slope_d}")
        print(f"Channel B - Intercept: {intercept_b}, Slope: {slope_b}")

        # Return only the slopes if that's the primary interest
        return slope_d, slope_b

    def plot_channels(self, name, color="red"):
        vbias_fine = np.linspace(self.vbias[0], self.vbias[-1], 800)
        p_mean_d = Polynomial.fit(self.vbias, self.means["D"], 1)
        p_mean_b = Polynomial.fit(self.vbias, self.means["B"], 1)
        plt.plot(self.vbias, self.means["D"], 'o', label=f'{name} - kanal D', color=color)
        plt.plot(self.vbias, self.means["B"], 's', label=f'{name} - kanal B', color=color)
        plt.plot(vbias_fine, p_mean_d(vbias_fine), '-', color=color)
        plt.plot(vbias_fine, p_mean_b(vbias_fine), '-', color=color)
        intercept_d, slope_d = p_mean_d.convert().coef
        intercept_b, slope_b = p_mean_b.convert().coef

        # Debug: Print the coefficients to verify
        print(f"Channel d - Intercept: {intercept_d}, Slope: {slope_d}")
        print(f"Channel B - Intercept: {intercept_b}, Slope: {slope_b}")

        # Return only the slopes if that's the primary interest
        return slope_d, slope_b

    def save(self, filename):
        """Saves the current instance to a file using pickle."""
        with open(filename, 'wb') as file:
            pickle.dump(self, file)

    @staticmethod
    def get_from_file(filename):
        """Loads a FitRatioManager instance from a file."""
        with open(filename, 'rb') as file:
            return pickle.load(file)



measBias = {}


folder = "desktop_app/data_amp_new/"
folder_pkl = "picled_objects/"
names = ["amp30", "amp305", "amp31"]
# for name in names:
#     vbias = []
#     channel_means = {'A': [], 'B': [], 'C': [], 'D': []}
#     channel_stds = {'A': [], 'B': [], 'C': [], 'D': []}
#     x_means = []
#     x_stds = []
#     y_means = []
#     y_stds = []
#     for i in range(3360, 3650, 20):
#         try:
#             filename = f"{name}_bias_{i}.dat"
#             meas = MeasurementObject(filename, folder)
#             meas.get_data_from_binary()
#         except FileNotFoundError:
#             continue
#         xratio, yratio = meas.get_single_ratios_normed()
#         center_value, width_value = get_gauss_mean_std(xratio, show=False)
#         x_means.append(center_value)
#         x_stds.append(width_value)
#         print(
#             f"Vbias: {i} V, X ratio, Fit Mean: {center_value:.4f}, Fit Std Dev: {width_value:.4f}, Ratio: {center_value / width_value:.4f} ")
#
#         center_value, width_value = get_gauss_mean_std(yratio)
#         y_means.append(center_value)
#         y_stds.append(width_value)
#
#         for channel in ['A', 'B', 'C', 'D']:
#             signal = np.array(meas.min_signal[f"Channel {channel}"])
#             signal = signal * 2.9 / 4095
#             values = signal
#             center_value, width_value = get_gauss_mean_std(signal)
#             channel_means[channel].append(center_value)
#             channel_stds[channel].append(width_value)
#
#             # ------------------------------
#
#
#
#             print(f"Vbias: {i} V, Channel: {channel}, Fit Mean: {center_value:.4f}, Fit Std Dev: {width_value:.4f}, Ratio: {center_value / width_value:.4f} ")
#         vbias.append(i)
#     channel_means["A"] = np.array(channel_means["A"])
#     channel_means["B"] = np.array(channel_means["B"])
#     channel_means["C"] = np.array(channel_means["C"])
#     channel_means["D"] = np.array(channel_means["D"])
#     channel_stds["A"] = np.array(channel_stds["A"])
#     channel_stds["B"] = np.array(channel_stds["B"])
#     channel_stds["C"] = np.array(channel_stds["C"])
#     channel_stds["D"] = np.array(channel_stds["D"])
#     vbias = [i * 11 / 4095 * 2.9 for i in vbias]
#     vbias = np.array(vbias)
#     meas1 = MeasuremnetsBias(name, vbias, channel_means, channel_stds)
#     meas1.add_x_data(x_means, x_stds)
#     meas1.add_y_data(y_means, y_stds)
#     measBias[name] = meas1
#
# for name in names:
#     filename = f"{name}.pkl"
#     measBias[name].save(filename)

measBias = {}
for name in names:
    filename = f"{folder_pkl}{name}.pkl"
    measBias[name] = MeasuremnetsBias.get_from_file(filename)

# measBias["amp30"].plot_ratios("red")
# measBias["amp305"].plot_ratios("blue")
# measBias["amp31"].plot_ratios("green")
# measBias["amp30"].plot_stds("red")
# measBias["amp305"].plot_stds("blue")
# measBias["amp31"].plot_stds("green")
# print(measBias["amp31"].means["A"])
# print(measBias["amp30"].means["A"])
# measBias["amp30"].plot_ratios_propagated("S1")
# measBias["amp305"].plot_ratios_propagated("S1")
#
#
# plt.cla()
# measBias["amp30"].plot_ratios("r1", 'red')
# measBias["amp305"].plot_ratios("r2", "blue")
# plt.legend(loc='upper left')
#
# plt.show()


plt.cla()
# plt.title("Primerjava odzivov pri razli;ni osvetlitvi in yaporni n")
kc_1a, kb_1a = measBias["amp30"].plot_channels("$P_1$", 'red')
kc_2a, kb_2a = measBias["amp305"].plot_channels("$P_2$", "blue")
#kc_3, kb_3 = measBias["amp31"].plot_channels("S3", "green")

plt.ylabel('Napetost [V]')
plt.xlabel("Zaporna napetost [V]")

plt.legend(loc='upper left')
plt.savefig('amp_bias.pgf')



plt.show()

plt.cla()
# plt.title("Primerjava odzivov pri razli;ni osvetlitvi in yaporni n")
kc_1s, kb_1s = measBias["amp30"].plot_stds("$P_1$", 'red')
kc_2s, kb_2s = measBias["amp305"].plot_stds("$P_2$", "blue")

plt.ylabel('Standardni odklon [V]')
plt.xlabel("Zaporna napetost [V]")

plt.legend(loc='upper left')
plt.savefig('std_bias.pgf')
print("ratios")
print(kc_1a / kc_2a, kb_1a / kb_2a)
print(kc_1s / kc_2s, kb_1s / kb_2s)
print(kc_1a / kc_1s, kb_1a / kb_1s)
print(kc_2a / kc_2s, kb_2a / kb_2s)
print("------------------")


plt.show()

plt.cla()
# plt.title("Primerjava odzivov pri razli;ni osvetlitvi in yaporni n")
kc_1, kb_1 = measBias["amp30"].plot_stds_relative("$P_1$", 'red')
kc_2, kb_2 = measBias["amp305"].plot_stds_relative("$P_2$", "blue")

plt.ylabel('Standardni odklon / Amplituda [%]')
plt.xlabel("Zaporna napetost [V]")

plt.legend(loc='upper left')
plt.savefig('relative_bias.pgf')


plt.cla()
measBias["amp30"].plot_ratios("$P_1$", 'red')
measBias["amp305"].plot_ratios("$P_2$", "blue")


plt.ylabel('Razmerje')
plt.xlabel("Zaporna napetost [V]")
plt.legend(loc='lower left')
plt.savefig('ratio_amp.pgf')

plt.show()

plt.cla()

measBias["amp30"].plot_ratios_std("$P_1$", 'red')
measBias["amp305"].plot_ratios_std("$P_2$", "blue")


plt.ylabel('Standardni odklon')
plt.xlabel("Zaporna napetost [V]")
plt.legend(loc='lower left')
plt.savefig('ratio_std.pgf')

plt.show()



# plt.cla()
#
# vbias = [i * 11 / 4095 * 2.9 for i in vbias]
# # Plotting polynomial fits for all channels
# colors = {'A': 'blue', 'B': 'red', 'C': 'green', 'D': 'purple'}
# vbias_fine = np.linspace(24.5, 29, 800)
# mean_fine = np.linspace(0, 4095, 800)
# channel_means["A"] = np.array(channel_means["A"])
# channel_means["B"] = np.array(channel_means["B"])
# channel_means["C"] = np.array(channel_means["C"])
# channel_means["D"] = np.array(channel_means["D"])
# ratiox = (channel_means["B"] - channel_means["D"])/ (channel_means["B"] + channel_means["D"])
# ratioy = (channel_means["A"] - channel_means["C"]) / (channel_means["A"] + channel_means["C"])
# plt.plot(vbias, ratiox, 'o', label=f'Ratio x', color="green")
# plt.plot(vbias, ratioy, 'o', label=f'Ratio x', color="blue")
#
# # Plot for Means
# plt.figure(figsize=(10, 6))
# # for channel, color in colors.items():
# #     p_mean = Polynomial.fit(vbias, channel_means[channel], 1)
# #     plt.plot(vbias, channel_means[channel], 'o', label=f'{channel} Means', color=color)
# #     plt.plot(vbias_fine, p_mean(vbias_fine), '-', color=color)
# plt.xlabel('Vbias (V)')
# plt.ylabel('Mean Values')
# plt.title('Mean Value Fits Across Channels')
# plt.legend()
# plt.show()
#
# # Plot for Standard Deviations
# plt.figure(figsize=(10, 6))
# for channel, color in colors.items():
#     p_std = Polynomial.fit(channel_means[channel], channel_stds[channel], 1)
#     plt.plot(channel_means[channel], channel_stds[channel], 's', label=f'{channel} Std Devs', color=color)
#     plt.plot(mean_fine, p_std(mean_fine), '--', color=color)
# plt.xlabel('Vbias (V)')
# plt.ylabel('Standard Deviation Values')
# plt.title('Standard Deviation Fits Across Channels')
# plt.legend()
# plt.show()
