import sys

from PyQt5.QtWidgets import QApplication

from data_gather.lion_window import LionWindow
from data_gather.tcp_client import LionClient
from fit_functions.fit_manager import FitRatioManager
from solution import SolutionManager

app = QApplication(sys.argv)
server_ip = '192.168.1.200'  # Replace with your server's IP
server_port = 10001  # Replace with your server's port

x_fit_manager = FitRatioManager.get_from_file('picled_objects/x_fit_manager_new.pkl')
y_fit_manager = FitRatioManager.get_from_file('picled_objects/y_fit_manager_new.pkl')
sol_manager = SolutionManager.load_grid_from_file(1000, "picled_objects/solution_manager_lion.pkl")
sol_manager.add_fit_x(x_fit_manager)
sol_manager.add_fit_y(y_fit_manager)
client = LionClient(server_ip, server_port)
window = LionWindow(client, sol_manager)
window.show()
sys.exit(app.exec_())