
# LION-Software


**LION-Software** is a Python-based project for the positional ion detector LION. It consists of two main parts: software to interface with the detector and software to analyze the results to determine ion positions. The software is currently in development and is intended for use with macro scripts, with examples provided in the root directory.

This project was developed as part of my Master Thesis. The firmware for the detector can be found [here](https://gitlab.com/-/ide/project/m.pozun99/lion-firmware).

## Desktop App

To interact with the LION detector, we developed an interface that connects to the Ethernet module on the detector via a TCP/IP socket, creating a serial connection. Communication with the detector can be done through scripts using the client interface directly or via a graphical user interface (GUI) that integrates the client interface.

### Client Interface

The client interface is composed of the `LionClient` class, which includes functions to send commands to the detector. The protocol is specified by the firmware running on the microcontroller of the detector.

To use this script, create a `LionClient` object from the `tcp_client.py` file and provide the IP address and port of the Ethernet module on the detector. After establishing the connection, you can start sending commands to change the settings of the detector, start the measurement process, and save the results to a binary file. The following script demonstrates the usage:

```python
import time
from data_gather.tcp_client import LionClient

server_port = 10001
server_ip = '10.42.0.244'
FOLDER = "data_vbias/"
client = LionClient(server_ip, server_port)
client.connect_to_server()
client.set_vthresh_setting(2300)
for vbias in range(3340, 3630, 20):
    if client.set_vbias_setting(vbias):
        name = f"amplitude_vbias_{vbias}.dat"
        filename = FOLDER + name
        client.start_measurement(filename)
        time.sleep(10)
        client.stop_measurement()
        time.sleep(0.5)
        print(f"Success, File: {name}")
    else:
        print(f"Error {vbias}")
```

### Graphical User Interface

The graphical user interface (GUI) allows users to monitor the measurement process and settings in real-time. For plotting, we used the Matplotlib library, integrated into the GUI with other graphical objects from the PyQt library. The GUI also visualizes positions in the form of a heatmap, with plots on the sides showing the distribution of the coordinates. To enable real-time plotting and position determination, a calibrated model needs to be added to the application. The process of starting the GUI application is demonstrated in the macro script `makro_gui.py`.

![Lion Software](gui_screenshot.png)

## Analysis Software

To determine the positions of the ions from the measurements obtained from the detector, we created a model based on the laws of physics. This model is used to determine positions from signals measured at different positions.

### Measurements

There are two ways to store measurements. During the testing phase, we used a PicoScope to store the signal in the form of CSV files, which we processed to determine the amplitude of the sensor response. The other type of measurement is stored in binary files when data is obtained from the detector via a serial connection.

All functions implementing these functionalities are in the `measurement` module, where we also create groups of measurements for calibrating the detector.

### Model Calibration

To calibrate the model, we use the `lmfit` library, which employs a least squares algorithm to determine the best-fitting parameters for the given model. In the macro script `makro_lion.py`, you can see an example of how we calibrated the model and saved the results to a file for later access.