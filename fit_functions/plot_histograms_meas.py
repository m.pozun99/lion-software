import pandas as pd
import matplotlib
matplotlib.use('Qt5Agg')
import numpy as np
from datetime import datetime
# Load data from CSV file

from measurement.util_functions import save_python_data_to_file, get_python_data_from_file, calculate_deviation_of_ratio

FOLDER = "~/meas_waves/"

NUMBER_OF_MEASUREMENTS = 10000
cutoff_frequency = 15e6  # define your cutoff frequency
sampling_rate = 2.5e9  # define the sampling rate of your data

measurement_names = ["meas_01", "meas_05", "meas_15"]
def get_measurement_data(name):
    min_original_a = np.zeros(NUMBER_OF_MEASUREMENTS)
    min_filtered_a = np.zeros(NUMBER_OF_MEASUREMENTS)
    min_original_b = np.zeros(NUMBER_OF_MEASUREMENTS)
    min_filtered_b = np.zeros(NUMBER_OF_MEASUREMENTS)
    t1 = datetime.now()
    subfolder = f"{name}/"
    for i in range(NUMBER_OF_MEASUREMENTS):
        filename = f"{name}_{i + 1:0>5}.csv"
        CSV_FILE = FOLDER + subfolder + filename
        data = pd.read_csv(CSV_FILE, skiprows=1)
        data.columns = ['Time', 'Channel A',  "Channel B", "Channel C", "Channel D"]
        signal_array_a = data['Channel A'].to_numpy()
        signal_array_b = data['Channel B'].to_numpy()
        # filtered_signal_a = low_pass_filter(signal_array_a, cutoff_frequency, sampling_rate)
        # filtered_signal_b = low_pass_filter(signal_array_b, cutoff_frequency, sampling_rate)
        min_original_a[i] = np.min(signal_array_a)
        # min_filtered_a[i] = np.min(filtered_signal_a)
        min_original_b[i] = np.min(signal_array_b)
        # min_filtered_b[i] = np.min(filtered_signal_b)

    save_python_data_to_file(min_original_a, f"min_original_{name}_a.pk")
    # save_python_data_to_file(min_filtered_a, f"min_filtered_{name}_a.pk")
    save_python_data_to_file(min_original_b, f"min_original_{name}_b.pk")
    # save_python_data_to_file(min_filtered_b, f"min_filtered_{name}_b.pk")
    t2 = datetime.now()
    print(f"Time_passed {t2 - t1}")

# for name in measurement_names:
#     get_results()
# values = get_measurement_data()
# plot_histogram()

# for name in measurement_names:
#     get_measurement_data(name)
IMAGES_FOLDER = "histograms/"
for name in measurement_names:
    min_a = np.abs(get_python_data_from_file(f"min_original_{name}_a.pk"))
    min_b = np.abs(get_python_data_from_file(f"min_original_{name}_b.pk"))

    if name != "meas_01":
        min_a *= 1000
        min_b *= 1000
    # APPLY OFFSET
    A_OFFSET = 1
    B_OFFSET = 3
    min_a += A_OFFSET
    min_b += B_OFFSET
    mean_a = np.mean(min_a)
    mean_b = np.mean(min_b)
    # print(f"Ratio = {mean_b / mean_a} at measurement {name}")
    ratio = min_b / min_a
    print(f"{name}")
    calculate_deviation_of_ratio(min_a, min_b)
    AMPLITUDE_N_1 = 1.5
    min_a_ph = min_a / AMPLITUDE_N_1
    min_b_ph = min_b / AMPLITUDE_N_1
    print("Calculate for photons")
    calculate_deviation_of_ratio(min_a_ph, min_b_ph)
    # plot_histogram(f"{name}_a", min_a, filename=IMAGES_FOLDER + f"his_{name}_a.png")
    # plot_histogram(f"{name}_a", min_b, filename=IMAGES_FOLDER + f"his_{name}_b.png")
    # plot_histogram(f"{name}_ratio", ratio, filename=IMAGES_FOLDER + f"his_{name}_ratio.png")




