import numpy as np
import lmfit
import matplotlib.pyplot as plt


def calculate_angle_precise(l_dist, w_dist, d=4):
    fi1 = np.arctan((w_dist - d / 2) / l_dist)
    fi2 = np.arctan(l_dist / (w_dist + d / 2))
    angle = np.pi / 2 - fi1 - fi2
    return angle

def ratio_function_x_normed(xy, a, d0, y01, y02):
    x, y = xy
    d1 = np.sqrt(np.square(d0+x) + np.square(y - y01))
    d2 = np.sqrt(np.square(d0-x) + np.square(y - y02))
    return (a * d2 - d1) / (a * d2 + d1)

def ratio_function_x(xy, a, d0, y01, y02):
    x, y = xy
    d1 = np.sqrt(np.square(d0+x) + np.square(y - y01))
    d2 = np.sqrt(np.square(d0-x) + np.square(y - y02))
    return a * d2 / d1

def ratio_function_x_angle(xy, a, d0, l, y01, y02):
    x, y = xy
    w_dist1 = np.abs(d0 + x)
    l_dist1 = np.abs(y - y01)
    l_dist2 = np.abs(y - y02)
    w_dist2 = np.abs(d0 - x)

    dist1 = np.sqrt(np.square(d0+x) + np.square(y - y01))
    dist2 = np.sqrt(np.square(d0-x) + np.square(y - y02))
    angle1 = calculate_angle_precise(w_dist1, l_dist1)
    angle2 = calculate_angle_precise(w_dist2, l_dist2)
    loss1 = np.exp(-dist1 / l)
    loss2 = np.exp(-dist2 / l)

    return a * angle1 * loss1 / angle2 / loss2


def ratio_function_x_angle_changed_vectorised(xy, a, d0, l, y01, y02):
    # Assume xy is an Nx2 array where xy[:, 0] is x and xy[:, 1] is y
    x, y = xy[:, 0], xy[:, 1]

    w_dist1 = np.abs(d0 + x)
    l_dist1 = np.abs(y - y01)
    l_dist2 = np.abs(y - y02)
    w_dist2 = np.abs(d0 - x)

    dist1 = np.sqrt(np.square(d0 + x) + np.square(y - y01))
    dist2 = np.sqrt(np.square(d0 - x) + np.square(y - y02))

    angle1 = calculate_angle_precise(w_dist1, l_dist1)
    angle2 = calculate_angle_precise(w_dist2, l_dist2)

    loss1 = np.exp(-dist1 / l)
    loss2 = np.exp(-dist2 / l)

    nominator = a * angle1 * loss1 - angle2 * loss2
    denominator = a * angle1 * loss1 + angle2 * loss2

    return nominator / denominator

def ratio_function_angle_normed(xy, a, d0, l, y01, y02, x0):
    x, y = xy
    x = x - x0
    w_dist1 = np.abs(d0 + x)
    l_dist1 = np.abs(y - y01)
    l_dist2 = np.abs(y - y02)
    w_dist2 = np.abs(d0 - x)

    dist1 = np.sqrt(np.square(d0+x) + np.square(y - y01))
    dist2 = np.sqrt(np.square(d0-x) + np.square(y - y02))
    angle1 = calculate_angle_precise(w_dist1, l_dist1)
    angle2 = calculate_angle_precise(w_dist2, l_dist2)
    loss1 = np.exp(-dist1 / l)
    loss2 = np.exp(-dist2 / l)
    nominator = a * angle1 * loss1 - angle2 * loss2
    denominator = a * angle1 * loss1 + angle2 * loss2
    return nominator / denominator

def ratio_function_angle_normed_old(xy, a, d0, l, y01, y02):
    x, y = xy
    w_dist1 = np.abs(d0 + x)
    l_dist1 = np.abs(y - y01)
    l_dist2 = np.abs(y - y02)
    w_dist2 = np.abs(d0 - x)

    dist1 = np.sqrt(np.square(d0+x) + np.square(y - y01))
    dist2 = np.sqrt(np.square(d0-x) + np.square(y - y02))
    angle1 = calculate_angle_precise(w_dist1, l_dist1)
    angle2 = calculate_angle_precise(w_dist2, l_dist2)
    loss1 = np.exp(-dist1 / l)
    loss2 = np.exp(-dist2 / l)
    nominator = a * angle1 * loss1 - angle2 * loss2
    denominator = a * angle1 * loss1 + angle2 * loss2
    return nominator / denominator

# Define your model function
def function_1_x(x, a, b):
    return a / (x - b)

def function_1_dist(xy, a, b, c):
    x, y = xy
    d = np.sqrt((x - b)**2 + (y - c)**2)
    return a / d

def function_1_dist_e(xy, a, b, c, d0):
    x, y = xy
    d = np.sqrt((x - b)**2 + (y - c)**2)
    return a / d * np.exp(-d/d0)

def dist(xy, b, c):
    x, y = xy
    return np.sqrt((x - b)**2 + (y - c)**2)

# Define the function to perform the fit
def fit_function(x_values, y_values, init_params, filename=None, show=False):
    # Create a Parameters object to hold the fit parameters
    sorted_indices = np.argsort(x_values)
    sorted_x_values = x_values[sorted_indices]
    sorted_y_values = y_values[sorted_indices]
    params = lmfit.Parameters()
    params.add('a', value=init_params["a"])
    params.add('b', value=init_params["b"])

    # Create a Model object for your model function
    model = lmfit.Model(function_1_x)

    # Fit the model to the data
    result = model.fit(data=sorted_y_values, params=params, x=sorted_x_values)

    # Print the fitting report
    print(result.fit_report())
    x_interp = np.linspace(sorted_x_values.min(), sorted_x_values.max(), len(sorted_x_values) * 100)
    # Return the fitted parameters
    fitted_params = result.params
    y_interp = result.eval(params=fitted_params, x=x_interp)

    plt.figure(figsize=(8, 6))
    plt.scatter(sorted_x_values, sorted_y_values, label='Data', color='blue')
    #plt.plot(sorted_x_values, result.best_fit, label='Fit', color='red')
    plt.plot(x_interp, y_interp, label='Fit', color='red')

    plt.xlabel('X')
    plt.ylabel('Y')
    plt.legend()
    if filename is not None:
        plt.savefig(filename, format='png', bbox_inches='tight')

    if show:
        plt.show()

    return fitted_params



def fit_ratio_function(x_values, y_values, ratio_values, init_params, filename=None, show=False):
    # Create a Parameters object to hold the fit parameters
    params = lmfit.Parameters()
    params.add('a', value=init_params["a"])
    params.add('d0', value=init_params["d0"], min=60.5, max=110)
    params.add('y01', value=init_params["y01"], min=-20, max=20, vary=True)
    params.add('y02', value=init_params["y02"], min=-20, max=20, vary=True)
    params.add('x0', value=init_params["x0"], min=-20, max=20, vary=True)


    # Combine x and y values into a single structure
    xy_values = (x_values, y_values)

    # Create a Model object for your model function
    model = lmfit.Model(ratio_function_x_normed)
    # model = lmfit.Model(ratio_function_x_angle)



    # Fit the model to the data
    result = model.fit(data=ratio_values, params=params, xy=xy_values)
    return model, result

def fit_ratiox_function_precise(x_values, y_values, ratio_values, init_params, filename=None, show=False):
    # Create a Parameters object to hold the fit parameters
    params = lmfit.Parameters()
    params.add('a', value=init_params["a"], min=0.7, max=1.5, vary=True)
    params.add('d0', value=init_params["d0"], min=70, max=110, vary=True)
    params.add('l', value=init_params["l"], min=10, max=1000000, vary=True)
    params.add('y01', value=init_params["y01"], min=-5, max=5, vary=False)
    params.add('y02', value=init_params["y02"], min=-5, max=5, vary=False)


    # params.add('d0', value=init_params["d0"], min=10, max=1000000000)


    # Combine x and y values into a single structure
    xy_values = (x_values, y_values)

    # Create a Model object for your model function
    model = lmfit.Model(ratio_function_angle_normed)
    # model = lmfit.Model(ratio_function_x_angle)



    # Fit the model to the data
    result = model.fit(data=ratio_values, params=params, xy=xy_values)
    return model, result

def fit_ratioy_function(x_values, y_values, ratio_values, init_params, filename=None, show=False):
    # Create a Parameters object to hold the fit parameters
    params = lmfit.Parameters()
    params.add('a', value=init_params["a"])
    params.add('d0', value=init_params["d0"], vary=True)
    # params.add('d0', value=init_params["d0"], min=10, max=1000000000)


    # Combine x and y values into a single structure
    xy_values = (x_values, y_values)

    # Create a Model object for your model function
    model = lmfit.Model(ratio_function_y)

    # Fit the model to the data
    result = model.fit(data=ratio_values, params=params, xy=xy_values)
    return model, result



def fit_dist_function(x_values, y_values, signal_values, init_params, filename=None, show=False):
    # Create a Parameters object to hold the fit parameters
    params = lmfit.Parameters()
    params.add('a', value=init_params["a"])
    params.add('b', value=init_params["b"], vary=True)
    params.add('c', value=init_params["c"], vary=True)
    # params.add('d0', value=init_params["d0"], min=10, max=1000000000)

    # Combine x and y values into a single structure
    xy_values = (x_values, y_values)

    # Create a Model object for your model function
    model = lmfit.Model(function_1_dist)

    # Fit the model to the data
    result = model.fit(data=signal_values, params=params, xy=xy_values)
    return model, result