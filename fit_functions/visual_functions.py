import matplotlib.pyplot as plt
import numpy as np
import matplotlib
from matplotlib.patches import Patch
import matplotlib
# matplotlib.use("pgf")
# matplotlib.rcParams.update({
#     "pgf.texsystem": "pdflatex",  # Ensures pdflatex is used
#     'font.family': 'serif',
#     'text.usetex': True,          # Enable LaTeX rendering
#     'pgf.rcfonts': False,         # Do not setup fonts from rc parameters
#     'text.latex.preamble': r'\usepackage{type1ec}',  # Ensures type1ec is loaded
# })

def plot_histogram(name, values, show=False, filename=None):
    plt.cla()
    count, bins, ignored = plt.hist(values, bins=32, density=False, alpha=0.6, color='g')
    bin_centers = (bins[:-1] + bins[1:]) / 2
    # Filter out bins with zero counts
    # non_zero_bins = count > 0
    # bin_centers_non_zero = bin_centers[non_zero_bins]
    # count_non_zero = count[non_zero_bins]
    center = np.mean(values)
    width = np.std(values)
    info = f"Fit results: mean = {center:.4f}, std = {width:.4f}"
    title = f'Histogram of Minimum Values {name}\n{info}'
    plt.title(title)
    plt.xlabel('Minimum Value of Signal')
    plt.ylabel('Frequency')
    plt.grid(True)
    if filename is not None:
        plt.savefig(filename, format='png', bbox_inches='tight')
    if show:
        plt.show()


def plot_multiple_histograms(names, values_list, bins_number=64, show=False, filename=None, zoom_factor=2,
                             outlier_threshold=5):
    plt.cla()
    num_datasets = len(values_list)

    # Creating a subplot for each dataset
    fig, axs = plt.subplots(2, 2, figsize=(10, 10))

    for i in range(num_datasets):
        values = values_list[i] * 2.9 / 4095  # Convert bin centers to voltage
        name = names[i]
        j = i // 2
        k = i % 2
        axis = axs[j][k]
        # Calculating mean and standard deviation
        mean = np.mean(values)
        std = np.std(values)

        # Filtering out outliers
        filtered_values = [x for x in values if abs(x - mean) <= outlier_threshold * std]
        std_old = std
        std = np.std(filtered_values)

        # Plotting histogram for each dataset
        count, bins, ignored = axis.hist(filtered_values, bins=bins_number, density=False, alpha=0.6, color='g')
        bin_centers = (bins[:-1] + bins[1:]) / 2
        kanal = name[-1]
        info = f"S = {mean:.3f} V, s = {std:.3f} V,  s / S (%) = {std / mean * 100:.3f} % "
        title = f'Histogram kanal {kanal}\n{info}'

        axis.set_title(title)
        axis.set_xlabel('Napetost [V]')
        axis.set_ylabel('Frekvenca')
        axis.grid(True)

    plt.tight_layout()

    if filename is not None:
        plt.savefig(filename)

    if show:
        plt.show()


def scatter_values(coord, data, name, filename=None, show=False):
    plt.figure(figsize=(12, 6))
    plt.subplot(1, 1, 1)
    for channel, amp in data.items():
        plt.scatter(coord, amp, label=channel)
    plt.xlabel('Coord')
    plt.ylabel('Amplitude')
    plt.title(name)
    plt.legend()
    plt.tight_layout()
    if filename is not None:
        plt.savefig(filename, format='png', bbox_inches='tight')

    if show:
        plt.show()


def plot_on_grid(acc, prec, filename=None, show=False):
    # Initialize lists for x, y coordinates and colors for points and circles
    x, y, colors, circle_colors = [], [], [], []

    # Extract x, y coordinates and values from filenames
    for key, value in acc.items():
        pos_x = int(key.split('_')[1])
        pos_y = int(key.split('_')[2])
        x.append(pos_x)
        y.append(pos_y)
        # Determine the color based on the absolute value of the point from acc
        abs_value = abs(value)
        if abs_value < 5:
            colors.append('green')
        elif abs_value < 10:
            colors.append('orange')
        elif abs_value < 15:
            colors.append('red')
        else:
            colors.append('black')

        # Determine the circle color based on the absolute value of the point from prec
        prec_value = abs(prec[key])
        if prec_value < 5:
            circle_colors.append('green')
        elif prec_value < 10:
            circle_colors.append('orange')
        elif prec_value < 15:
            circle_colors.append('red')
        else:
            circle_colors.append('black')

    # Plotting
    plt.figure(figsize=(10, 8))
    for i in range(len(x)):
        plt.scatter(x[i], y[i], edgecolors=circle_colors[i], facecolors='none', s=400, linewidths=4)  # Circles around points
        plt.scatter(x[i], y[i], c=colors[i], s=130)  # Plotting the points

    plt.grid(True)
    plt.xticks(range(-60, 61, 15))
    plt.yticks(range(-60, 61, 15))
    plt.axhline(0, color='gray', linewidth=0.5)
    plt.axvline(0, color='gray', linewidth=0.5)
    # plt.title("Grid Points Plot with Accuracy and Precision")
    plt.xlabel("x [mm]")
    plt.ylabel("y [mm]")

    # Manually create a legend
    legend_elements = [
        Patch(facecolor='green', edgecolor='none', label='$E < $5 mm'),
        Patch(facecolor='orange', edgecolor='none', label='$E< $10 mm'),
        Patch(facecolor='red', edgecolor='none', label='$E < $15 mm'),
        Patch(facecolor='black', edgecolor='none', label='$E >= $15 mm'),
        Patch(facecolor='none', edgecolor='green', label='$s < $5 mm', linewidth=4),
        Patch(facecolor='none', edgecolor='orange', label='$s < $15 mm', linewidth=4),
        Patch(facecolor='none', edgecolor='red', label='$s < $15 mm', linewidth=4),
        Patch(facecolor='none', edgecolor='black', label='$s >= $15 mm', linewidth=4)
    ]
    plt.legend(handles=legend_elements, loc='lower left', bbox_to_anchor=(1, 0), title='Legenda')
    if filename is not None:
        #plt.savefig(filename)
        plt.savefig(filename, format='png', dpi=300, bbox_inches='tight')
    if show:
        plt.show()
