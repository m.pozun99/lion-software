import pickle
from datetime import datetime

import numpy as np
from scipy.optimize import minimize
from lmfit import Minimizer, Parameters

from fit_functions.fit_functions import ratio_function_x_normed, ratio_function_angle_normed
from fit_functions.fit_manager import FitRatioManager

class SerializableFunction:
    """
    A class to encapsulate a function and optionally switch its arguments before calling.
    This class is serializable and can handle dynamic function creation based on provided parameters.
    """
    def __init__(self, func_info, switch=False):
        self.func_info = func_info
        self.switch = switch
        self.parameters = func_info['parameters']
        self.function_type = func_info['function_type']

    def __call__(self, xy):
        if self.switch:
            xy = [xy[1], xy[0]]  # Switch the order of the xy coordinates if required

        # Call the appropriate function based on the function type
        if self.function_type == "1/x":
            return ratio_function_x_normed(xy, **self.parameters)
        elif self.function_type == "angle":
            return ratio_function_angle_normed(xy, **self.parameters)
        else:
            raise ValueError("Unsupported function type")

    def __getstate__(self):
        # This method is called during pickling. Customize what gets pickled.
        state = self.__dict__.copy()
        # Optionally remove or modify unpickleable entries here (not needed in this case)
        return state

    def __setstate__(self, state):
        # This method is called during unpickling.
        self.__dict__.update(state)



class SolutionManager:
    def __init__(self, num_points=50):
        self.num_points = num_points
        self.ratios = np.linspace(-1, 1, self.num_points)
        self.x_coords = np.zeros((self.num_points, self.num_points))
        self.y_coords = np.zeros((self.num_points, self.num_points))

    def add_fit_x(self, ratiox_fit):
        self.ratiox_fit = ratiox_fit
        self.ratio_function_x = SerializableFunction(ratiox_fit.get_function())

    def add_fit_y(self, ratioy_fit):
        self.ratioy_fit = ratioy_fit
        self.ratio_function_y = SerializableFunction(ratioy_fit.get_function(), switch=True)

    def create_eq(self, func, switch=False):
        if switch:
            return lambda xy: func([xy[1], xy[0]])  # Make sure input is a list or array appropriately
        else:
            return func

    def solve_ratios(self, Rx, Ry, table=False):
        n_data = len(Rx)
        results_x = np.zeros(n_data)
        results_y = np.zeros(n_data)

        for i in range(n_data):
            result = self.solve_single_ratio(Rx[i], Ry[i], table=table)
            results_x[i] = result[0]
            results_y[i] = result[1]

        return results_x, results_y

    def solve_single_ratio(self, Rx, Ry, table=False):
        # Define the initial guess and bounds
        if table:
            return self.find_closest_x_y(Rx, Ry)
        else:
            initial_guess = np.array([0, 0])
            bounds = [(-70, 70), (-70, 70)]

            # Define the objective function for a single pair (Rx, Ry)
            def objective(xy):
                x, y = xy
                residual_x = (self.ratio_function_x([x, y]) - Rx) ** 2
                residual_y = (self.ratio_function_y([x, y]) - Ry) ** 2
                return residual_x + residual_y

            # Minimize the objective function
            result = minimize(objective, initial_guess, bounds=bounds, method='L-BFGS-B')
            return result.x

    def estimate_unc(self, Rx, stdx, Ry, stdy):
        n_samples = 200
        Rx_samples = np.random.normal(Rx, stdx, n_samples)
        Ry_samples = np.random.normal(Ry, stdy, n_samples)
        results = np.array([self.solve_single_ratio(Rx_sample, Ry_sample) for Rx_sample, Ry_sample in zip(Rx_samples, Ry_samples)])
        x_samples, y_samples = results[:, 0], results[:, 1]
        return np.std(x_samples, axis=0), np.std(y_samples, axis=0)

    def calculate_coordinates(self):
        for i, ratio1 in enumerate(self.ratios):
            for j, ratio2 in enumerate(self.ratios):
                x, y = self.solve_single_ratio(ratio1, ratio2)
                self.x_coords[i, j] = x
                self.y_coords[i, j] = y

    def find_closest_x_y(self, ratio1, ratio2):
        # Calculate the index of the closest ratio based on the known spacing
        idx1 = int((ratio1 + 1) * (self.num_points - 1) / 2)
        idx2 = int((ratio2 + 1) * (self.num_points - 1) / 2)
        try:
            closest_x = self.x_coords[idx1, idx2]
            closest_y = self.y_coords[idx1, idx2]
        except IndexError as e:
            print(ratio1, ratio2)
            print(e)
            closest_x = np.NAN
            closest_y = np.NAN

        return closest_x, closest_y

    def save_grid_to_file(self, filename='grid_data.pkl'):
        # Save only the grid data to file
        with open(filename, 'wb') as file:
            grid_data = {'x_coords': self.x_coords, 'y_coords': self.y_coords}
            pickle.dump(grid_data, file)
        print(f"Grid data saved to {filename}")

    @classmethod
    def load_grid_from_file(cls, number_of_points, filename):
        # Load grid data from file
        with open(filename, 'rb') as file:
            grid_data = pickle.load(file)
        new_manager = cls(number_of_points)
        new_manager.x_coords = grid_data['x_coords']
        new_manager.y_coords = grid_data['y_coords']
        return new_manager





if __name__ == "__main__":
    x_fit_manager = FitRatioManager.get_from_file('../picled_objects/x_fit_manager_new.pkl')
    y_fit_manager = FitRatioManager.get_from_file('../picled_objects/y_fit_manager_new.pkl')
    # sol_manager = SolutionManager(num_points=1000)
    # sol_manager.add_fit_x(x_fit_manager)
    # sol_manager.add_fit_y(y_fit_manager)
    # t1 = datetime.now()
    # sol_manager.calculate_coordinates()
    # t2 = datetime.now()
    # print(t2 - t1)
    # sol_manager.save_grid_to_file("solution_manager_lion.pkl")
    sol_manager = SolutionManager.load_grid_from_file(1000, "../picled_objects/solution_manager_lion.pkl")
    sol_manager.add_fit_x(x_fit_manager)
    sol_manager.add_fit_y(y_fit_manager)
    print(sol_manager.ratio_function_x((11,1.5)))
    print(sol_manager.ratio_function_y((11, 1.5)))
    print(sol_manager.find_closest_x_y(-0.2, 0.18))
    print(sol_manager.solve_single_ratio(-0.2, 0.18))





# Example usage:
#
# sol_manager = SolutionManager(x_fit_manager, y_fit_manager)
# Rx, Ry = np.array([...]), np.array([...])
# positions = sol_manager.solve_ratios(Rx, Ry)
