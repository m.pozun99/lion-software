import pickle

import lmfit
import numpy as np
from matplotlib import pyplot as plt
import matplotlib
# matplotlib.use("pgf")
# matplotlib.rcParams.update({
#     "pgf.texsystem": "pdflatex",
#     'font.family': 'serif',
#     'text.usetex': True,
#     'pgf.rcfonts': False,
# })

from fit_functions.fit_functions import ratio_function_x_normed, ratio_function_angle_normed, ratio_function_x_angle_changed_vectorised


class FitRatioManager:
    def __init__(self, function_type, init_params, x_values, y_values):
        if function_type == "1/x":
            self.fit_function = ratio_function_x_normed
            self.fit_function_vectorized = ratio_function_x_normed
        elif function_type == "angle":
            self.fit_function = ratio_function_angle_normed
            self.fit_function_vectorized = ratio_function_x_angle_changed_vectorised

        else:
            raise ValueError
        self.function_type = function_type
        self.init_params = init_params
        self.x_values = x_values
        self.y_values = y_values
        self.model = None
        self.result = None

    def fit(self, ratio_values):
        if self.function_type == "1/x":
            self.fit_ratio_function(self.x_values, self.y_values, ratio_values, self.init_params)
        elif self.function_type == "angle":
            self.fit_ratio_function_angle(self.x_values, self.y_values, ratio_values, self.init_params)

    # def get_function(self):
    #     if self.function_type == "1/x":
    #         params = self.result.params
    #         a = params['a'].value
    #         d0 = params['d0'].value
    #         y01 = params['y01'].value
    #         y02 = params['y02'].value
    #
    #         def f(xy):
    #             return self.fit_function(xy, a, d0, y01, y02)
    #         return f
    #     elif self.function_type == "angle":
    #         params = self.result.params
    #         a = params['a'].value
    #         d0 = params['d0'].value
    #         l = params['l'].value
    #         y01 = params['y01'].value
    #         y02 = params['y02'].value
    #         x0 = params['x0'].value
    #         def f(xy):
    #             return self.fit_function(xy, a, d0, l, y01, y02, x0)
    #         return f

    def get_function(self):
        """ Returns the parameters and the function type needed to recreate the function elsewhere."""
        if not self.result:
            raise ValueError("Fit has not been performed or results are not available.")

        params = {name: param.value for name, param in self.result.params.items()}
        return {
            'function_type': self.function_type,
            'parameters': params
        }


    def fit_ratio_function(self, x_values, y_values, ratio_values, init_params):
        params = lmfit.Parameters()
        params.add('a', value=init_params["a"])
        params.add('d0', value=init_params["d0"], min=60.1, max=110)
        params.add('y01', value=init_params["y01"], min=-20, max=20, vary=True)
        params.add('y02', value=init_params["y02"], min=-20, max=20, vary=True)
        xy_values = (x_values, y_values)
        # Create a Model object for your model function
        model = lmfit.Model(ratio_function_x_normed)
        result = model.fit(data=ratio_values, params=params, xy=xy_values)
        self.model = model
        self.result = result
        return model, result

    def fit_ratio_function_angle(self, x_values, y_values, ratio_values, init_params, filename=None, show=False):
        # Create a Parameters object to hold the fit parameters
        params = lmfit.Parameters()
        params.add('a', value=init_params["a"], min=0.7, max=1.5, vary=True)
        params.add('d0', value=init_params["d0"], min=60.1, max=120, vary=True)
        params.add('l', value=init_params["l"], min=10, max=1000000, vary=True)
        params.add('y01', value=init_params["y01"], min=-10, max=10, vary=True)
        params.add('y02', value=init_params["y02"], min=-10, max=10, vary=True)
        params.add('x0', value=init_params["x0"], min=-5, max=5, vary=True)

        # Combine x and y values into a single structure
        xy_values = (x_values, y_values)

        # Generate weights where each weight is 1, but 0.5 if any coordinate is 60 or more
        weights = np.ones_like(x_values)
        # weights[(x_values >= 45) | (y_values >= 45)] = 0.6
        weights[(x_values >= 60) | (y_values >= 60)] = 0.6
        # Create a Model object for your model function
        model = lmfit.Model(ratio_function_angle_normed)
        # Fit the model to the data
        result = model.fit(data=ratio_values, params=params, xy=xy_values, weights=weights)
        self.model = model
        self.result = result
        return model, result


    def scatter_plot_ratio(self, forward_values, side_values, side_value, ratios, model, color, tag, style="-", x=True):
        indexes = np.where(side_values == side_value)[0]
        ratios_index= ratios[indexes]
        forward_values_index = forward_values[indexes]
        # side_values_index = side_values[indexes]
        plt.scatter(forward_values_index, ratios_index, color=color)
        x_interp = np.linspace(-60, 60, 500)
        y_interp = np.ones_like(x_interp) * side_value
        xy_interp = (x_interp, y_interp)
        fitted_interpolated = model.eval(params=self.result.params, xy=xy_interp)
        if x:
            label = f"y = {side_value:4d} mm"
        else:
            label = f"x = {side_value:4d} mm"
        plt.plot(x_interp, fitted_interpolated, linestyle=style, color=color, label=label)

    def plot_ratios(self, ratio, title, style, clear=True, show=True, filename=None, x=True):
        # Define a color palette
        colors = ["green", "orange", "purple", "blue", "red", "gold", "black", "magenta", "cyan"]
        side_values = [0, 30, -30, 60, -60]
        color_map = dict(zip(side_values, colors))

        if clear:
            plt.cla()

        for side_value in side_values:
            self.scatter_plot_ratio(self.x_values, self.y_values, side_value, ratio, self.model, color=color_map.get(side_value, "gray"), style=style, tag=title, x=x)
        if x:
            plt.xlabel('$x [mm]$')
        else:
            plt.xlabel('$y [mm]$')
        plt.ylabel("Razmerje")
        plt.legend()
        # if filename is not None:
        #     plt.savefig(filename)

        if show:
            plt.title(title)
            plt.legend()
            plt.show()


    def save(self, filename):
        """Saves the current instance to a file using pickle."""
        with open(filename, 'wb') as file:
            pickle.dump(self, file)

    @staticmethod
    def get_from_file(filename):
        """Loads a FitRatioManager instance from a file."""
        with open(filename, 'rb') as file:
            return pickle.load(file)

